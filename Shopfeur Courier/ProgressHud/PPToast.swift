//
//  PPToast.swift
//  Adfile
//
//  Created by Prabhat Pankaj on 31/12/15.
//  Copyright © 2015 Prabhat Pankaj. All rights reserved.
//

import Foundation

import Foundation

var HUD = MBProgressHUD()

extension MBProgressHUD {
    
    class func showHUDMessage(message:String,PPview:UIView){
        
        
        if !(HUD.isEqual(nil)) {
            PPview.isUserInteractionEnabled = true
            HUD = MBProgressHUD(view: PPview)
            PPview.addSubview(HUD)
        }
        HUD.graceTime = 0.0
        HUD.labelText = "Please wait..."
        HUD.detailsLabelText = message
       // HUD.color = UIColor(red: 179/255, green: 10/255, blue: 28/255, alpha: 1.0)
        
        //HUD.mode = MBProgressHUDMode.AnnularDeterminate
        HUD.animationType = MBProgressHUDAnimation.zoomIn
        HUD.show(true)
    }
    
    
    class  func showToast(message:String, name:String, PPView:UIView){
        
        let HUD = MBProgressHUD(view: PPView)
        PPView.addSubview(HUD!)
        
        // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
        // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
        HUD?.customView =  UIImageView(image: UIImage(named: name))
        // HUD.customView = UIImageView(frame: CGRectMake(0, 0, 80, 80))
        
        // Set custom view mode
        HUD?.mode = MBProgressHUDMode.customView
        // CGContextSetRGBFillColor (context,218.0f/255.0f, 218.0f/255.0f, 218.0f/255.0f, 0.9f);
        //CGContextSetRGBFillColor (context,255.0f/255.0f, 75.0f/255.0f, 72.0f/255.0f, 0.9f);
       // HUD?.color = UIColor(red: 179/255, green: 10/255, blue: 28/255, alpha: 1.0)
        HUD?.detailsLabelText = message
        HUD?.detailsLabelColor = UIColor.lightGray
        HUD?.show(true)
        HUD?.hide(true, afterDelay: 2)
        
    }
    
//    class  func showDashBoardToast(message:String, name:String, PPView:UIView){
//        
//        var HUD = MBProgressHUD(view: PPView)
//        
//        PPView.addSubview(HUD)
//        
//        // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
//        // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
//        HUD.customView =  UIImageView(image: UIImage(named: name))
//        // HUD.customView = UIImageView(frame: CGRectMake(0, 0, 80, 80))
//        
//        // Set custom view mode
//        HUD.mode = MBProgressHUDMode.CustomView
//        HUD.color = UIColor(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0)
//        HUD.detailsLabelText = message
//        //  HUD.detailsLabelFont = UIFont(name: "HelveticaNeue", size: 20)
//        HUD.show(true)
//        HUD.hide(true, afterDelay: 2)
//        
//    }
    
    
    class func hideHUD() {
        HUD.hide(true)
        HUD.removeFromSuperview()
        
    }
}
