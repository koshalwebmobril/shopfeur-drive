//
//  SignupEntities.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import ObjectMapper

private let KEY_ERROR = "error";
private let KEY_MESSAGE = "message";
private let KEY_RESULT = "result";

class SignupEntities: Mappable{
    internal var error: Bool?;
    internal var message: String?;
    internal var result: NSDictionary?;
    
    required init?(map: Map) {
        mapping(map: map);
    }
    
    func mapping(map: Map) {
        error <- map[KEY_ERROR];
        message <- map[KEY_MESSAGE];
        result <- map[KEY_RESULT];
    }
    
}
