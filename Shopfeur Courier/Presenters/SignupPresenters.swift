//
//  SignupPresenters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation

class SignupPresenters: SignupViewToPresenterProtocol {
     
    var view: SignupPresenterToViewProtocol?;
    var interector: SignupPresentorToInterectorProtocol?;
    var router: SignupPresenterToRouterProtocol?
    
    func callSignupAPI(name: String,mobileNumber: String,password: String,imageData: Data) {
        interector?.getSignup(name: name, mobileNumber: mobileNumber,password: password,imageData:imageData);
    }
}

extension SignupPresenters: SignupInterectorToPresenterProtocol {
     
    func apiFailedNoInternet(){
        view?.noInternet()
    }
    
    func apiError(msg: String) {
        view?.showAlert(title: "Error", msg: msg)
    }
    
    func Signupdeatilsfetched(dict: SignupEntities) {
        view?.fetchSignupdata(dict: dict);
    }
}
