//
//  ForgotPasswordPresenters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 14/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation

class ForgotPasswordPresenters: ForgotPasswordViewToPresenterProtocol {
     
    var view: ForgotPasswordPresenterToViewProtocol?;
    var interector: ForgotPasswordPresentorToInterectorProtocol?;
    var router: ForgotPasswordPresenterToRouterProtocol?
    
    func callForgotPasswordAPI(mobileNumber: String) {
        interector?.getForgotPassword(mobileNumber: mobileNumber);
    }
    
    func callResendOTPAPI(mobileNumber: String) {
           interector?.getForgotPassword(mobileNumber: mobileNumber);
       }
}

extension ForgotPasswordPresenters: ForgotPasswordInterectorToPresenterProtocol {
    
    func forgotdeatilsfetched(dict: ForgotPasswordEntities) {
        view?.fetchforgotdata(dict: dict);
    }
    
    func apiFailedNoInternet(){
        view?.noInternet()
    }
    
    func apiError(msg: String) {
        view?.showAlert(title: "Error", msg: msg)
    }
}
