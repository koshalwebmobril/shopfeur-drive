//
//  SignInPresenters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 12/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation

class SignInPresenters: SignInViewToPresenterProtocol {
     
    var view: SignInPresenterToViewProtocol?;
    var interector: SignInPresentorToInterectorProtocol?;
    var router: SignInPresenterToRouterProtocol?
    
    func callLoginAPI(mobileNumber: String, Password: String) {
        interector?.getSignIn(mobileNumber: mobileNumber, Password: Password);
    }
}

extension SignInPresenters: SignInInterectorToPresenterProtocol {
    
    func logindeatilsfetched(dict: SignInEntities) {
        view?.fetchdata(dict: dict);
    }
    
    func apiFailedNoInternet(){
        view?.noInternet()
    }
    
    func apiError(msg: String) {
        view?.showAlert(title: "Error", msg: msg)
    }
}
