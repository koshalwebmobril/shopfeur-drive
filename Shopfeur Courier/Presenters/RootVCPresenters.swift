//
//  RootVCPresenters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 16/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation

class RootVCPresenters: RootVCViewToPresenterProtocol {
     
    var view: RootVCPresenterToViewProtocol?;
    var interector: RootVCPresentorToInterectorProtocol?;
    var router: RootVCPresenterToRouterProtocol?
}

extension RootVCPresenters: RootVCInterectorToPresenterProtocol {
    
}
