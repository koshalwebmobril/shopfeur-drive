//
//  OTPScreenPresenters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation

class OTPScreenPresenters: OTPScreenViewToPresenterProtocol {
     
    var view: OTPScreenPresenterToViewProtocol?;
    var interector: OTPScreenPresentorToInterectorProtocol?;
    var router: OTPScreenPresenterToRouterProtocol?
    
    func callOTPVerifyAPI(OTP: String) {
        interector?.getOTPVerified(OTP: OTP)
    }
}

extension OTPScreenPresenters: OTPScreenInterectorToPresenterProtocol {
    func OTPdeatilsfetched(dict: OTPScreenEntities) {
        view?.fetchOTPdata(dict: dict);
    }
    
    func apiFailedNoInternet(){
        view?.noInternet()
    }
    
    func apiError(msg: String) {
        view?.showAlert(title: "Error", msg: msg)
    }
}
