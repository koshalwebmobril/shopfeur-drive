//
//  AppExtensionVC.swift
//  Shopfeur Users
//
//  Created by deepkohli on 11/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import PopupDialog


let REGEX_USER_PHONE="^[0-9]{7,15}$"

class AppExtensionVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension UILabel {
func textDropShadow() {
    self.layer.masksToBounds = false
    self.layer.shadowRadius = 3.0
    self.layer.shadowOpacity = 0.8
    self.layer.shadowOffset = CGSize(width: 1, height: 2)
}
}

extension String {
    
    var removingWhitespacesAndNewlines: String {
        return components(separatedBy: .whitespacesAndNewlines).joined()
    }
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[0-9][0-9]{6}([0-9]{1})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
    func isValidPhoneNumber() -> Bool {
           
           let regex = try! NSRegularExpression(pattern: REGEX_USER_PHONE,options: .caseInsensitive)
           return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
       }
}

extension UIViewController {
    
    public func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
     
        // at least one uppercase
        // at least one digit
        // at least one lowercase
        // at lease one special symbol
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$")
        return passwordTest.evaluate(with: testStr)
    }
    
    public func concatAttributedString(firstString: String, secondString: String)->NSMutableAttributedString {
        let firstDateString = NSAttributedString(string: firstString, attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        let secondDateString = NSAttributedString(string: secondString, attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2235294118, green: 0.3215686275, blue: 0.7529411765, alpha: 1)])
        let resultAttributedString =  NSMutableAttributedString()
        resultAttributedString.append(firstDateString)
        resultAttributedString.append(secondDateString)
        return resultAttributedString
    }
    
    public func PasswordShowHide(textfield: UITextField)->Bool {
//        self.view.endEditing(true)
        if textfield.text == "" {
            statusBarErrorAlert(msg: AppConstants.EmptyPassword)
            return true
        } else if textfield.isSecureTextEntry {
            textfield.isSecureTextEntry = false
             return false
        } else {
            textfield.isSecureTextEntry = true
            return true
        }
    }
    
    
    
    public func statusBarErrorAlert(msg: String) {
        let banner = StatusBarNotificationBanner(title: msg, style: .danger)
        banner.show()
    }
    
    public func statusBarSuccessAlert(msg: String) {
        let banner = StatusBarNotificationBanner(title: msg, style: .success)
        banner.show()
    }
    
    public func showAlertDialog(title: String, description: String, buttonCount: Int? = 0) {
        let title = title
        let message = description
        
        let popup = PopupDialog(title: title, message: message, preferredWidth: 480)

        if buttonCount == 2 {
            let buttonOne = CancelButton(title: "Ok") { [weak self] in
                                  
            }
            
            let buttonTwo = DefaultButton(title: "Cancel") { [weak self] in
                       
            }
            popup.addButtons([buttonOne, buttonTwo])
          
        } else {
            let buttonOne = CancelButton(title: "Cancel") { [weak self] in
                          
                      }
                      popup.addButtons([buttonOne])
        }
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
