//
//  APPUrls.swift
//  Shopfeur Users
//
//  Created by deepkohli on 11/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit

class APPUrls: NSObject {
    
    // MARK: - Base Url
    static let BaseUrl = "https://www.webmobril.org/dev/shopfeur/api/v1/"
    static let ImageBaseUrl = "http://webmobril.org/dev/shopfeur/storage/"
    
    // MARK: - API
    static let splashurl = "splashes"
    static let signup = "auth/register-as-driver"
    static let signin = "auth/login-as-driver"
    static let forgotPassword = "auth/forgot-password"
    static let verifyOTP = "auth/confirm-otp"
    static let logout = "auth/logout"
}
