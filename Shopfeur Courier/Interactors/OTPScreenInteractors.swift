//
//  OTPScreenInteractors.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class OTPScreenInteractors: OTPScreenPresentorToInterectorProtocol{
     
    var presenter: OTPScreenInterectorToPresenterProtocol?;
    
    func getOTPVerified(OTP: String) {
         if Reachability.isConnectedToNetwork() {
            let mobileNumber = UserDefaults.standard.value(forKey: "MOBILENUMBER") as? String ?? ""
            let urlString = String(format : APPUrls.BaseUrl + APPUrls.verifyOTP)
            let params: [String: Any] = ["mobile":mobileNumber,"otp":OTP]
        
                Alamofire.request(urlString, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                    switch response.result {
                        case .success:
                            let json = response.result.value as? [String: Any]
//                            print(json!)
                            let dictObject = Mapper<OTPScreenEntities>().map(JSON: json!)
                            self.presenter?.OTPdeatilsfetched(dict: dictObject!)
                            break
        
                        case .failure(let error):
//                            print(error)
                            self.presenter?.apiError(msg: "\(error)")
                            }
                        }
                } else {
                    self.presenter?.apiFailedNoInternet()
                }
    }
    
}
