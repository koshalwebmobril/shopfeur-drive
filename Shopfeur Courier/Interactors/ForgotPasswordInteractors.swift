//
//  ForgotPasswordInteractors.swift
//  Shopfeur Users
//
//  Created by deepkohli on 14/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ForgotPasswordInteractors: ForgotPasswordPresentorToInterectorProtocol{
     
    var presenter: ForgotPasswordInterectorToPresenterProtocol?;
    
    func getForgotPassword(mobileNumber: String) {
         if Reachability.isConnectedToNetwork() {
                let urlString = String(format : APPUrls.BaseUrl + APPUrls.forgotPassword)
                let params: [String: Any] = ["mobile":mobileNumber]
        
                Alamofire.request(urlString, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                    
                    switch response.result {
                        case .success:
                            let json = response.result.value as? [String: Any]
//                            print(json!)
                            let dictObject = Mapper<ForgotPasswordEntities>().map(JSON: json!)
                            self.presenter?.forgotdeatilsfetched(dict: dictObject!)
                            break
        
                        case .failure(let error):
//                            print(error)
                            self.presenter?.apiError(msg: "\(error)")
                            }
                        }
                } else {
                    self.presenter?.apiFailedNoInternet()
                }
    }
}
