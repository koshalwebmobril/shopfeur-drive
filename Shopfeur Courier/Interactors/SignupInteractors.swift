//
//  SignupInteractors.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class SignupInteractors: SignupPresentorToInterectorProtocol{
     
    var presenter: SignupInterectorToPresenterProtocol?;
    
    func getSignup(name: String,mobileNumber: String,password: String,imageData: Data) {
         if Reachability.isConnectedToNetwork() {
            let urlString = String(format : APPUrls.BaseUrl + APPUrls.signup)
            let params: [String: Any] = ["first_name":name,"mobile":mobileNumber,"password":password,"password_confirmation":password,"is_term_accept":"0","device_type":"IPhone","device_token":"Not Available"]
        
            Alamofire.upload(multipartFormData: { multipartFormData in
                           multipartFormData.append(imageData, withName: "image",fileName: "profile.jpg", mimeType: "image/jpg")
                           for (key, value) in params {
                               multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                           }
            }, to:urlString) { (result) in
                
                switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
                        })
                        upload.responseJSON { response in
                            let json = response.result.value as? [String: Any]
//                            print(json!)
                            let dictObject = Mapper<SignupEntities>().map(JSON: json!)
                            self.presenter?.Signupdeatilsfetched(dict: dictObject!)
                    }
                    
                    case .failure(let encodingError):
                        print(encodingError)
                        }
                    }
                } else {
                    self.presenter?.apiFailedNoInternet()
                }
    }
}
