//
//  SignInInteractors.swift
//  Shopfeur Users
//
//  Created by deepkohli on 12/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class SignInInteractors: SignInPresentorToInterectorProtocol{
     
    var presenter: SignInInterectorToPresenterProtocol?;
    
    func getSignIn(mobileNumber: String, Password: String) {
         if Reachability.isConnectedToNetwork() {
            let urlString = String(format : APPUrls.BaseUrl + APPUrls.signin)
            let params: [String: Any] = ["user_name":mobileNumber,"password":Password,"device_type":"2","device_token":"Not Available"]
//            print(urlString)
//            print(params)
                Alamofire.request(urlString, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
                    switch response.result {
                        case .success:
                            let json = response.result.value as? [String: Any]
//                            print(json!)
                            let dictObject = Mapper<SignInEntities>().map(JSON: json!)
                            self.presenter?.logindeatilsfetched(dict: dictObject!)
                            break
        
                        case .failure(let error):
//                            print(error)
                            self.presenter?.apiError(msg: "\(error)")
                            }
                        }
                } else {
                    self.presenter?.apiFailedNoInternet()
                }
    }
}
