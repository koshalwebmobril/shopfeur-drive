//
//  apiList.swift
//  Ziphawk_Child
//
//  Created by Sushant on 17/07/19.
//  Copyright © 2019 WebMobril. All rights reserved.
//

import Foundation

struct GlobalURL {
    
//    http://www.shopfeur.com/api/v1/
    static let baseURL = "https://www.shopfeur.com/api/v1/auth/"
    static let baseURL1 = "https://www.shopfeur.com//api/v1/"
    static let imagebaseURL = "https://www.shopfeur.com//storage/"
    
    static let loginURL = "login-as-driver"
    static let signUpURL = "register-as-driver"
    static let forgotPasswordURL = "forgot-password-driver"
    static let verifyOtpURL = "confirm-otp-driver"
    static let logoutURL = "logout"
    static let driverProfileURL = "driver-profile"
    static let uploadDocumentURL = "add_driver_information"
    static let addVehicleInfoURL = "add_vehicle_information"
    static let orderNotificationsURL = "order_notifications"
    static let orderHistoryURL = "driver_orders"
    static let orderAcceptRejectURL = "order_accept_reject"
    static let uploadOrderImageURL = "upload_order_images"
    static let userOrderDetailsURL = "api_user_orders_details"
    static let uploadOtherDocumentsURL = "other_documents_upload"
    static let orderDetailsURL = "api_user_orders_details"
    static let getVehicleTypeURL = "api_vehicle_type"
    static let driverStatus = "api_ready_for_pickup"
    static let outForDelivery = "driver_shipped_order"
    static let driverDeliveredItem = "driver_delivered_order"
    static let helpAndSupport = "api_support"
    static let driverAssignedOrderOrParcel = "api_order_parcel_assign_to_driver"
    
    static let getLastNotification = "api-ios-pending-notifications"
    static let allPendingNotifications = "api-all-pending-notifications"
    
    static let orderRejectURL = "api-reject-notifications"
    static let resetPasswordURL = "reset-password"
    
}
