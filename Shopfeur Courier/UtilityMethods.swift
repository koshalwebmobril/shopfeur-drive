//
//  UtilityMethods.swift
//  TaxiVIP_Driver
//
//  Created by Sushant on 21/10/19.
//  Copyright © 2019 webmobril. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class UtilityMethods {
    
    private static var indicatorCount = 0
    private static let activityData = ActivityData(size: CGSize(width: UIScreen.main.bounds.size.width/10, height: UIScreen.main.bounds.size.width/10), messageFont: UIFont(name: "System", size: 15), messageSpacing: 5, type: .ballRotateChase, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), backgroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.4), textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    
    static func showIndicator(withMessage message: String? = nil) {
        if indicatorCount == 0 {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
        }
        indicatorCount += 1
    }
    
    static func hideIndicator() {
        indicatorCount -= 1
        indicatorCount == 0 ? NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil) : ()
    }
}
