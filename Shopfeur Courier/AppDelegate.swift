//
//  AppDelegate.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 01/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import UserNotifications
import AKSideMenu
import DropDown
import Firebase
import Messages
import UserNotifications

let deviceType = 2

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    
    var window: UIWindow?
    var token = String()
    
    
    static let sharedInstance = UIApplication.shared.delegate as! AppDelegate
    
    let webviewvc = UIStoryboard(name: AppConstants.MainStoryboard, bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
    let profilevc = UIStoryboard(name: AppConstants.MainStoryboard, bundle: nil).instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
    let helpsupportvc = UIStoryboard(name: AppConstants.MainStoryboard, bundle: nil).instantiateViewController(withIdentifier: "HelpSupportVC") as! HelpSupportVC
    let notificationvc = UIStoryboard(name: AppConstants.MainStoryboard, bundle: nil).instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        firebase()
        registerForPushNotifications()
        
        IQKeyboardManager.shared.enable = true
        //        setLoginAsRoot()
        
        DropDown.startListeningToKeyboard()
        
        application.applicationIconBadgeNumber = 0
        
        Thread.sleep(forTimeInterval: 5)
        
        let sharedApplication = UIApplication.shared
        sharedApplication.delegate?.window??.tintColor = .init(red: 93/255.0, green: 122/255.0, blue: 248/255.0, alpha: 1.0)
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: (sharedApplication.delegate?.window??.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = .init(red: 93/255.0, green: 122/255.0, blue: 248/255.0, alpha: 1.0)
            sharedApplication.delegate?.window??.addSubview(statusBar)
        } else {
            // Fallback on earlier versions
            sharedApplication.statusBarView?.backgroundColor = .init(red: 93/255.0, green: 122/255.0, blue: 248/255.0, alpha: 1.0)
        }
        
        
        /*
         let signinvc = SignInRouters.createModule();
         window = UIWindow(frame: UIScreen.main.bounds);
         window?.rootViewController = signinvc
         window?.makeKeyAndVisible();
         */
        
        Fabric.with([Crashlytics.self])
        
        if UserDefaults.standard.object(forKey: "user_Id") == nil {
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryBoard.instantiateViewController(withIdentifier: "SignInVc") as? SignInVc
            let navigationController = UINavigationController(rootViewController: controller!)
            navigationController.isNavigationBarHidden = true
            window!.rootViewController = navigationController
            window!.makeKeyAndVisible()
            
            
        } else {
            
            
            //  setLoginAsRoot()
            
            //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            //            let controller = mainStoryBoard.instantiateViewController(withIdentifier: "OtherDocumentsVC") as? OtherDocumentsVC
            //            let navigationController = UINavigationController(rootViewController: controller!)
            //            navigationController.isNavigationBarHidden = true
            //            window!.rootViewController = navigationController
            //            window!.makeKeyAndVisible()
            
            
        }
        
        
        
        return true
    }
    
    func firebase(){
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let temptoken = tokenParts.joined()
//        print("Device Token: \(temptoken)")
        token = temptoken
        
        UserDefaults.standard.set(token, forKey: "device_Token")
        UserDefaults.standard.synchronize()
        
    }
    
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
//            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                //                UNUserNotificationCenter.current().delegate = self
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("FCM token: \(fcmToken)")
        
        let fToken = fcmToken
        UserDefaults.standard.set(fToken, forKey: "fcm_token")
        UserDefaults.standard.synchronize()
    }
    
    
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
//            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        

        
        UserDefaults.standard.setValue("FromNotification", forKey: "Notification")
        



        if let jsonString = userInfo[AnyHashable("gcm.notification.data")] as? String,

            let data = jsonString.data(using: .utf8) {

            do {
                if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String : Any] {
                    
                    var notificationType = String()
                    var finalDict = [String:Any]()
                    if json["result"] as? [String:Any] != nil{
                        finalDict = json["result"] as! [String : Any]
                        
                        UserDefaults.standard.setValue(finalDict["data"], forKey: "data")
                        
                    }
                }
                
            }catch{

            }
        }
        

        NotificationCenter.default.post(name: NSNotification.Name("Notifiaction"), object: nil, userInfo: userInfo)

        
        
    }
    
   
    
    func setLoginAsRoot() {
        // Create content and menu controllers
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: OrderHistoryVC.init())
        let leftMenuViewController = LeftmenuVC() //= LeftmenuVC.init()
        
        
        // Create side menu controller
        //        let sideMenuViewController = AKSideMenu(contentViewController: navigationController, leftMenuViewController: LeftmenuVC.self)
        
        let side = AKSideMenu(contentViewController: navigationController, leftMenuViewController: leftMenuViewController, rightMenuViewController: nil)
        
        // Make it a root controller
        self.window!.rootViewController = side
        
        self.window!.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Shopfeur_Courier")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - Call View
    
    func callRootViewController() {
        let rootvc = RootVCRouters.createModule()
        window = UIWindow(frame: UIScreen.main.bounds);
        window?.rootViewController = rootvc
        window?.makeKeyAndVisible();
    }
    
    func callSignupController() {
        let signup = SignupRouters.createModule()
        window = UIWindow(frame: UIScreen.main.bounds);
        window?.rootViewController = signup
        window?.makeKeyAndVisible();
    }
    
    func callSigninController() {
        let signinvc = SignInRouters.createModule();
        window = UIWindow(frame: UIScreen.main.bounds);
        window?.rootViewController = signinvc
        window?.makeKeyAndVisible();
    }
    
    func callForgotController() {
        let forgotvc = ForgotPasswordRouters.createModule()
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(forgotvc, animated: true, completion: nil)
    }
    
    func callVerifyOTPController() {
        let otpvc = OTPScreenRouters.createModule()
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(otpvc, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    func callProfileVC() {
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(profilevc, animated: true, completion: nil)
    }
    
    func callOrders() {
        
    }
    
    func callNotifications(){
        notificationvc.labeltitle = "Notifications"
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(notificationvc, animated: true, completion: nil)
    }
    
    func callHelpSupportController() {
        helpsupportvc.labeltitle = "Support"
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(helpsupportvc, animated: true, completion: nil)
    }
    
    func callPrivacyPolicyController() {
        webviewvc.labeltitle = "Privacy Policy"
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(webviewvc, animated: true, completion: nil)
    }
    
    func callAboutus() {
        webviewvc.labeltitle = "About us"
        let currentcontroller = UIApplication.shared.keyWindow?.rootViewController
        currentcontroller?.present(webviewvc, animated: true, completion: nil)
    }
    
    
}


extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
