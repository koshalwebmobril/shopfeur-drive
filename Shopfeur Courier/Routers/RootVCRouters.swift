//
//  RootVCRouters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 16/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit
import AKSideMenu

class RootVCRouters: RootVCPresenterToRouterProtocol {
    
    class func createModule() ->AKSideMenu{
        if #available(iOS 13.0, *) {
            let view = mainstoryboard.instantiateViewController(withIdentifier: "rootController") as? RootViewController;
            let presenter: RootVCViewToPresenterProtocol & RootVCInterectorToPresenterProtocol = RootVCPresenters();
            let interactor: RootVCPresentorToInterectorProtocol = RootVCInteractors();
            let router: RootVCPresenterToRouterProtocol = RootVCRouters();
                       
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        } else {
            // Fallback on earlier versions
            let view = mainstoryboard.instantiateViewController(withIdentifier: "rootController") as? RootViewController;
            let presenter: RootVCViewToPresenterProtocol & RootVCInterectorToPresenterProtocol = RootVCPresenters();
            let interactor: RootVCPresentorToInterectorProtocol = RootVCInteractors();
            let router: RootVCPresenterToRouterProtocol = RootVCRouters();
            
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        }
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
    }
}
