//
//  SignInRouters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 12/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

class SignInRouters: SignInPresenterToRouterProtocol{
    
    class func createModule() ->UIViewController{
        if #available(iOS 13.0, *) {
            let view = mainstoryboard.instantiateViewController(withIdentifier: "SignInVc") as? SignInVc;
            let presenter: SignInViewToPresenterProtocol & SignInInterectorToPresenterProtocol = SignInPresenters();
            let interactor: SignInPresentorToInterectorProtocol = SignInInteractors();
            let router: SignInPresenterToRouterProtocol = SignInRouters();
                       
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        } else {
            // Fallback on earlier versions
            let view = mainstoryboard.instantiateViewController(withIdentifier: "SignInVc") as? SignInVc;
            let presenter: SignInViewToPresenterProtocol & SignInInterectorToPresenterProtocol = SignInPresenters();
            let interactor: SignInPresentorToInterectorProtocol = SignInInteractors();
            let router: SignInPresenterToRouterProtocol = SignInRouters();
            
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        }
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
    }
}
