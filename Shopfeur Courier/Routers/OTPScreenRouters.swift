//
//  OTPScreenRouters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

class OTPScreenRouters: OTPScreenPresenterToRouterProtocol{
    
    class func createModule() ->UIViewController{
        if #available(iOS 13.0, *) {
            let view = mainstoryboard.instantiateViewController(withIdentifier: "OtpScreenVCs") as? OtpScreenVCs;
            let presenter: OTPScreenViewToPresenterProtocol & OTPScreenInterectorToPresenterProtocol = OTPScreenPresenters();
            let interactor: OTPScreenPresentorToInterectorProtocol = OTPScreenInteractors();
            let router: OTPScreenPresenterToRouterProtocol = OTPScreenRouters();
                       
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        } else {
            // Fallback on earlier versions
            let view = mainstoryboard.instantiateViewController(withIdentifier: "OtpScreenVCs") as? OtpScreenVCs;
            let presenter: OTPScreenViewToPresenterProtocol & OTPScreenInterectorToPresenterProtocol = OTPScreenPresenters();
            let interactor: OTPScreenPresentorToInterectorProtocol = OTPScreenInteractors();
            let router: OTPScreenPresenterToRouterProtocol = OTPScreenRouters();
            
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        }
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
    }
}
