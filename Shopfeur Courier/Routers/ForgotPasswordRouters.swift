//
//  ForgotPasswordRouters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 14/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordRouters: ForgotPasswordPresenterToRouterProtocol{
    
    class func createModule() ->UIViewController{
        if #available(iOS 13.0, *) {
            let view = mainstoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVCs") as? ForgotPasswordVCs;
            let presenter: ForgotPasswordViewToPresenterProtocol & ForgotPasswordInterectorToPresenterProtocol = ForgotPasswordPresenters();
            let interactor: ForgotPasswordPresentorToInterectorProtocol = ForgotPasswordInteractors();
            let router: ForgotPasswordPresenterToRouterProtocol = ForgotPasswordRouters();
                       
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        } else {
            // Fallback on earlier versions
            let view = mainstoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVCs") as? ForgotPasswordVCs;
            let presenter: ForgotPasswordViewToPresenterProtocol & ForgotPasswordInterectorToPresenterProtocol = ForgotPasswordPresenters();
            let interactor: ForgotPasswordPresentorToInterectorProtocol = ForgotPasswordInteractors();
            let router: ForgotPasswordPresenterToRouterProtocol = ForgotPasswordRouters();
            
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        }
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
    }
}
