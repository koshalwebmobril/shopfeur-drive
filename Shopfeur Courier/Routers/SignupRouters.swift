//
//  SignupRouters.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

class SignupRouters: SignupPresenterToRouterProtocol{
    
    class func createModule() ->UIViewController{
        if #available(iOS 13.0, *) {
            let view = mainstoryboard.instantiateViewController(withIdentifier: "SignupVCs") as? SignupVCs;
            let presenter: SignupViewToPresenterProtocol & SignupInterectorToPresenterProtocol = SignupPresenters();
            let interactor: SignupPresentorToInterectorProtocol = SignupInteractors();
            let router: SignupPresenterToRouterProtocol = SignupRouters();
                       
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        } else {
            // Fallback on earlier versions
            let view = mainstoryboard.instantiateViewController(withIdentifier: "SignupVCs") as? SignupVCs;
            let presenter: SignupViewToPresenterProtocol & SignupInterectorToPresenterProtocol = SignupPresenters();
            let interactor: SignupPresentorToInterectorProtocol = SignupInteractors();
            let router: SignupPresenterToRouterProtocol = SignupRouters();
            
            view?.presenter = presenter;
            presenter.view = view;
            presenter.router = router;
            presenter.interector = interactor;
            interactor.presenter = presenter;
            return view!;
        }
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
    }
}
