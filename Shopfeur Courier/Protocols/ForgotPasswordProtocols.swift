//
//  ForgotPasswordProtocols.swift
//  Shopfeur Users
//
//  Created by deepkohli on 14/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

protocol ForgotPasswordPresenterToViewProtocol: class {
    func showAlert(title: String, msg: String);
    func fetchforgotdata(dict: ForgotPasswordEntities);
    func noInternet();
}

protocol ForgotPasswordInterectorToPresenterProtocol: class {
    func forgotdeatilsfetched(dict: ForgotPasswordEntities);
    func apiFailedNoInternet();
    func apiError(msg: String);
}

protocol ForgotPasswordPresentorToInterectorProtocol: class {
    var presenter: ForgotPasswordInterectorToPresenterProtocol? {get set} ;
    func getForgotPassword(mobileNumber: String);
}

protocol ForgotPasswordViewToPresenterProtocol: class {
    var view: ForgotPasswordPresenterToViewProtocol? {get set};
    var interector: ForgotPasswordPresentorToInterectorProtocol? {get set};
    var router: ForgotPasswordPresenterToRouterProtocol? {get set}
     func callForgotPasswordAPI(mobileNumber: String);
}

protocol ForgotPasswordPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController;
}
