//
//  RootVCProtocols.swift
//  Shopfeur Users
//
//  Created by deepkohli on 16/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit
import AKSideMenu

protocol RootVCPresenterToViewProtocol: class {
     
}

protocol RootVCInterectorToPresenterProtocol: class {
    
}

protocol RootVCPresentorToInterectorProtocol: class {
    var presenter: RootVCInterectorToPresenterProtocol? {get set} ;
    
}

protocol RootVCViewToPresenterProtocol: class {
    var view: RootVCPresenterToViewProtocol? {get set};
    var interector: RootVCPresentorToInterectorProtocol? {get set};
    var router: RootVCPresenterToRouterProtocol? {get set}
     
}

protocol RootVCPresenterToRouterProtocol: class {
    static func createModule() -> AKSideMenu;
}
