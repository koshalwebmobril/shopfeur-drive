//
//  OTPScreenProtocols.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

protocol OTPScreenPresenterToViewProtocol: class {
    func fetchOTPdata(dict: OTPScreenEntities);
    func showAlert(title: String, msg: String);
    func noInternet();
}

protocol OTPScreenInterectorToPresenterProtocol: class {
    func OTPdeatilsfetched(dict: OTPScreenEntities);
    func apiFailedNoInternet();
    func apiError(msg: String);
}

protocol OTPScreenPresentorToInterectorProtocol: class {
    var presenter: OTPScreenInterectorToPresenterProtocol? {get set} ;
    func getOTPVerified(OTP: String);
}

protocol OTPScreenViewToPresenterProtocol: class {
    var view: OTPScreenPresenterToViewProtocol? {get set};
    var interector: OTPScreenPresentorToInterectorProtocol? {get set};
    var router: OTPScreenPresenterToRouterProtocol? {get set}
    func callOTPVerifyAPI(OTP: String);
}

protocol OTPScreenPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController;
}
