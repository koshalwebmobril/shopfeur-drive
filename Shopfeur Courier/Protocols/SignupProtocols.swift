//
//  SignupProtocols.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

protocol SignupPresenterToViewProtocol: class {
    func fetchSignupdata(dict: SignupEntities);
    func showAlert(title: String, msg: String);
    func noInternet();
}

protocol SignupInterectorToPresenterProtocol: class {
    func Signupdeatilsfetched(dict: SignupEntities);
    func apiFailedNoInternet();
    func apiError(msg: String);
}

protocol SignupPresentorToInterectorProtocol: class {
    var presenter: SignupInterectorToPresenterProtocol? {get set} ;
    func getSignup(name: String,mobileNumber: String,password: String,imageData: Data);
}

protocol SignupViewToPresenterProtocol: class {
    var view: SignupPresenterToViewProtocol? {get set};
    var interector: SignupPresentorToInterectorProtocol? {get set};
    var router: SignupPresenterToRouterProtocol? {get set}
    func callSignupAPI(name: String,mobileNumber: String,password: String,imageData: Data);
}

protocol SignupPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController;
}
