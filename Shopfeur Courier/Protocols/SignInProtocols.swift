//
//  SignInProtocols.swift
//  Shopfeur Users
//
//  Created by deepkohli on 12/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation
import UIKit

protocol SignInPresenterToViewProtocol: class {
    func fetchdata(dict: SignInEntities);
    func showAlert(title: String, msg: String);
    func noInternet();
}

protocol SignInInterectorToPresenterProtocol: class {
    func logindeatilsfetched(dict: SignInEntities);
    func apiFailedNoInternet();
    func apiError(msg: String);
}

protocol SignInPresentorToInterectorProtocol: class {
    var presenter: SignInInterectorToPresenterProtocol? {get set} ;
    func getSignIn(mobileNumber: String, Password: String);
}

protocol SignInViewToPresenterProtocol: class {
    var view: SignInPresenterToViewProtocol? {get set};
    var interector: SignInPresentorToInterectorProtocol? {get set};
    var router: SignInPresenterToRouterProtocol? {get set}
    func callLoginAPI(mobileNumber: String, Password: String);
}

protocol SignInPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController;
}
