//
//  CreateNewPasswordVC.swift
//  Shopfeur Users
//
//  Created by deepkohli on 15/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CreateNewPasswordVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txt_newPasswordBackground: UITextField!
    @IBOutlet weak var txt_newpaassword: CustomTextField!
    @IBOutlet weak var txt_confirmnewPasswordBackground: UITextField!
    @IBOutlet weak var txt_confirmnewPassword: CustomTextField!
    @IBOutlet weak var btn_submit: UIButton!
    
    let otp = UserDefaults.standard.value(forKey: "otpforpassword") as? String ?? ""
    let number = UserDefaults.standard.value(forKey: "numberforpassword") as? String ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txt_newPasswordBackground.layer.cornerRadius = txt_newPasswordBackground.frame.height/2
        txt_newpaassword.setRightPaddingPoints(10)
        txt_newpaassword.setLeftPaddingPoints(10)
        txt_newpaassword.delegate = self
        txt_confirmnewPasswordBackground.layer.cornerRadius = txt_confirmnewPasswordBackground.frame.height/2
        txt_confirmnewPassword.setRightPaddingPoints(10)
        txt_confirmnewPassword.setLeftPaddingPoints(10)
        txt_confirmnewPassword.delegate = self
        btn_submit.layer.cornerRadius = btn_submit.frame.height/2
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated:true)
        
    }
    
    
    @IBAction func action_PasswordShowHide(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            if PasswordShowHide(textfield: txt_newpaassword) {
                txt_newpaassword.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
            } else {
                txt_newpaassword.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
            }
        default:
            if PasswordShowHide(textfield: txt_confirmnewPassword) {
                txt_confirmnewPassword.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
            } else {
                txt_confirmnewPassword.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
            }
        }
    }
    
    @IBAction func action_submit(_ sender: Any) {
        self.view.endEditing(true)
        if txt_newpaassword.text!.isEmpty {
            statusBarErrorAlert(msg: "Enter password")
        } else if txt_newpaassword.text!.count < 6 {
        statusBarErrorAlert(msg: "Password length should be atleast 6 character")
        }else if txt_confirmnewPassword.text!.isEmpty {
            statusBarErrorAlert(msg: "Enter confirm password")
        } else if !(txt_newpaassword.text == txt_confirmnewPassword.text) {
            statusBarErrorAlert(msg: "Password mismatch")
        } else {
            apiChangePassword()
        }
    }
    
    
    func apiChangePassword() {
    
        let apiChangePasswordURL = "\(GlobalURL.baseURL)\(GlobalURL.resetPasswordURL)"
//        print(apiChangePasswordURL)

    let newTodo: [String: Any] = ["mobile": number, "password": txt_newpaassword.text!, "password_confirmation": txt_confirmnewPassword.text!, "otp": otp]
//        print(newTodo)

//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)

        Alamofire.request(apiChangePasswordURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in

            MBProgressHUD.hideHUD()
            if (response.result.value != nil){
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()

                    let responceJson = swiftyJsonVar["result"]
//                    print(responceJson)

                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Password changed successfully"
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {action in
                        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
                        if let view = storyboard.instantiateViewController(withIdentifier: "SignInVc") as? SignInVc {
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)

                } else {
                    UtilityMethods.hideIndicator()

                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }

    }

    
}
