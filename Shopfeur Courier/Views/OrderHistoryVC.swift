//
//  OrderHistoryVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class OrderHistoryVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate ,CLLocationManagerDelegate{
   
    @IBOutlet weak var tableview_orderhistory: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var emptyImageView: UIImageView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    @IBOutlet weak var emptyView: UIView!
    
    var refreshControl = UIRefreshControl()
    
   
    @IBOutlet weak var documentMessage: UILabel!
    
    
    
    var arrOrders = JSON()
    var arrAssignData = JSON()
    var filterdata = [JSON]()
    var issearching = false
    lazy var searchBar = UISearchBar()
    
    var orderID = Int()
    var orderStatus = Int()
    var name = String()
    var address = String()
    var phone = String()
    var email = String()
    var amount = String()
    var city = String()
    var State = String()
    var Zipcode = String()
    var date = String()
    var driverStatus=String()
    var latitudeVal = Double()
    var longitudeVal = Double()
    var currentLocation = CLLocation()
    var locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        orderAssignedToDriver()
        
        UtilityMethods.hideIndicator()
        
//        getDriverProfile()
//        orderHistoryAPI()

        // Do any additional setup after loading the view.
       // tableview_orderhistory.tableFooterView = UIView()
        
        
        
                       

//
            
       
        
        tableview_orderhistory.backgroundView = UIImageView(image: #imageLiteral(resourceName: "viewbg"))
        tableview_orderhistory.delegate=self
        tableview_orderhistory.dataSource=self
        tableview_orderhistory.estimatedRowHeight=200
        tableview_orderhistory.rowHeight=UITableView.automaticDimension
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableview_orderhistory.addSubview(refreshControl)
            
            
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(popupNotification(_ :)), name: NSNotification.Name("Notifiaction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(orderHistoryAction(_ :)), name: NSNotification.Name("orderHistory"), object: nil)
        

    }
    
    @objc func orderHistoryAction(_ sender:Notification){
         MBProgressHUD.showHUDMessage(message: "Getting order for you...", PPview: self.view)
        self.orderHistoryAPI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        getLastNotification()
        
        if (UserDefaults.standard.value(forKey: "Notification") != nil){
            
            UserDefaults.standard.removeObject(forKey: "Notification")
            
            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickupRequestVC") as! PickupRequestVC
            popvc.popupDict = (UserDefaults.standard.value(forKey: "data") as? [String:Any])!
            popvc.modalPresentationStyle = .overCurrentContext
//            print( popvc.popupDict)
            self.present(popvc, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func popupNotification(_ sender:Notification){
        
         UserDefaults.standard.removeObject(forKey: "Notification")
        
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickupRequestVC") as! PickupRequestVC
        
        popvc.popupDict = (UserDefaults.standard.value(forKey: "data") as? [String:Any])!
//        print( popvc.popupDict)
        popvc.modalPresentationStyle = .overCurrentContext
        
        self.present(popvc, animated: true, completion: nil)
        
    }
    
    
    @objc func refresh() {
        MBProgressHUD.showHUDMessage(message: "Getting order for you...", PPview: self.view)
        self.orderHistoryAPI()
    }
    
    
    @objc func notificationAction () {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       
        let label = UILabel(frame: CGRect(x: 50, y: 20, width: self.view.frame.width-50.0, height: 40))
        label.text = "Orders History"
        label.textAlignment = .left
        self.navigationItem.titleView = label
       

        let notifications = UIBarButtonItem(image: UIImage(named: "Notifications"), style: .plain, target: self, action: #selector(notificationAction))
        let search = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchAction))

        navigationItem.rightBarButtonItems = [notifications, search]
        notifications.tintColor = UIColor.black
        search.tintColor = UIColor.black
        let attributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 24)!]
        UINavigationBar.appearance().titleTextAttributes = attributes
        self.navigationController?.isNavigationBarHidden = false
        
        bottomView.layer.masksToBounds=true
        bottomView.layer.cornerRadius=5
        
        setupLocationManager()
      
        
    }
    
     
    @objc func searchAction () {
        
        
        showSearchBar(searchBar: searchBar)

        
    }
    
    func setupLocationManager(){
    
    locManager.delegate = self
    locManager.desiredAccuracy = kCLLocationAccuracyBest
    locManager.requestWhenInUseAuthorization()

    locManager.startUpdatingLocation()
        

    if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
    CLLocationManager.authorizationStatus() == .authorizedAlways){
        
        self.bottomView.isHidden = false
        self.tableview_orderhistory.isHidden = false
        self.getDriverProfile()
        

//       print("location authorized......")
    }else{
        self.bottomView.isHidden = true
        let alert = UIAlertController(title: "Allow Location Access", message: "Shopfeur Drive needs access to your location to show the order List. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

        // Button to Open Settings
        alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                    print("Settings opened: \(success)")
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }


    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways: break
//            self.bottomView.isHidden = false
//            self.tableview_orderhistory.isHidden = false
//            self.getDriverProfile()
           
        case .restricted, .denied:
            
            self.bottomView.isHidden = true
                let alert = UIAlertController(title: "Allow Location Access", message: "Shopfeur Drive needs access to your location to show the order List. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                // Button to Open Settings
                alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                            print("Settings opened: \(success)")
                        })
                    }
                }))
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
          
            
        default:
        break
            
            
    }
    }
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//    print("locations = \(locations)")
//    mapView.showsUserLocation = true
    let location = locations.last!

    guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

    latitudeVal = locValue.latitude
    longitudeVal = locValue.longitude


    }
    
    func showSearchBar(searchBar : UISearchBar) {
        searchBar.alpha = 0
        navigationItem.rightBarButtonItems = nil
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        searchBar.tintColor = UIColor.black
        searchBar.placeholder = "Enter Order Id"
        searchBar.keyboardType = .numberPad
        
        
        self.searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        searchBar.showsCancelButton = true
        UIView.animate(withDuration: 0.3, animations: {
            searchBar.alpha = 1
        }, completion: { finished in
            searchBar.becomeFirstResponder()
        })
    }
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
//         self.navigationItem.title = "Order History"
        
        let notifications = UIBarButtonItem(image: UIImage(named: "Notifications"), style: .plain, target: self, action: #selector(notificationAction))
        let search = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchAction))
        
        navigationItem.rightBarButtonItems = [notifications, search]
        notifications.tintColor = UIColor.black
        search.tintColor = UIColor.black
        searchBar.isHidden = true
        
        self.navigationItem.titleView = nil
        let label = UILabel(frame: CGRect(x: 50, y: 20, width: self.view.frame.width-50.0, height: 40))
        label.text = "Orders History"
        label.textAlignment = .left
        self.navigationItem.titleView = label
//        self.navigationItem.title = "Order History"
//        self.title = "Orders"
//        self.navigationController?.title = "Orders"
        issearching = false
        filterdata.removeAll()
        tableview_orderhistory.reloadData()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        
    }
    

    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //        print(searchText)
        if searchText == "" {
//            searchBar.showsCancelButton = false
issearching = false
            tableview_orderhistory.reloadData()
        }
        else {
            
            filterdata.removeAll()
    
//            searchBar.showsCancelButton = true
            filterdata.removeAll()
            issearching = true
            
            for i in 0 ..< self.arrOrders.count {
                let temp = self.arrOrders[i]["order_id"].stringValue
                if (temp.contains(searchText)) {
                    self.filterdata.append(self.arrOrders[i])
                    
                } else {
                    
                    documentMessage.isHidden = false
                    documentMessage.text = "No records found"
                    
                    
                }
            }
            
            if self.filterdata.count>0{
                tableview_orderhistory.reloadData()
            }else{
                tableview_orderhistory.reloadData()
                
                let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Invalid Order Id"
                    , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if issearching {
            return filterdata.count
        }
           return self.arrOrders.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderHistoryCell = tableview_orderhistory.dequeueReusableCell(withIdentifier: "OrderHistoryCell", for: indexPath) as! OrderHistoryCell
        
        self.emptyView.isHidden = true
        
        cell.view_Orderhistory.layer.masksToBounds=true
        cell.view_Orderhistory.layer.cornerRadius=10
        
//        cell.btnAccept.isHidden=true
//        cell.btnReject.isHidden=true
        
        
        
        if issearching {
            let orderstatus = self.filterdata[indexPath.row]["order_status"].intValue
            
        
            if orderstatus == 1 {
                cell.lblStatus.text  = "NEW"
                
                
            } else if orderstatus == 2 {
                cell.lblStatus.text  = "ACCEPTED"
                
            } else if orderstatus == 3 {
                cell.lblStatus.text  = "OUT FOR DELIVERY"
               
            } else if orderstatus == 4 {
                cell.lblStatus.text  = "DELIVERED"
                
                
            }
            else if orderstatus == 5 {
                cell.lblStatus.text  = "CANCELLED"
                
                
            }
            else  {
                cell.lblStatus.text  = "REFUND"
                
                
            }
            
            if self.filterdata[indexPath.row]["order_type"].stringValue == "order"{
                
                
                cell.lblorderDelivery.text = "Order Delivery"
                let fullName = self.filterdata[indexPath.row]["billing_address"].stringValue + " " + self.filterdata[indexPath.row]["billing_city"].stringValue + " " + self.filterdata[indexPath.row]["billing_state"].stringValue + " " + self.filterdata[indexPath.row]["billing_zipcode"].stringValue
                cell.lblAddressDetails.text = "\(fullName)"
                
//                cell.lblAddressDetails.text = self.filterdata[indexPath.row]["billing_address"].stringValue
                cell.lblAddress.text = self.filterdata[indexPath.row]["store_address"].stringValue
                
            }
            else{
                
                cell.lblorderDelivery.text = "Parcel Delivery"
                
                cell.lblAddressDetails.text = self.filterdata[indexPath.row]["to_pickup_address"].stringValue
                cell.lblAddress.text = self.filterdata[indexPath.row]["from_pickup_address"].stringValue
                
            }
            
            cell.orderNumber.text = String(format: "Order No. : %ld", self.filterdata[indexPath.row]["order_id"].intValue)
            
            cell.lblPrice.text = String(format: "$%@", self.filterdata[indexPath.row]["delivery_charge"].stringValue)
            
            
            let lat1 = Double(self.filterdata[indexPath.row]["to_lat"].stringValue) ?? 0
            let long1 = Double(self.filterdata[indexPath.row]["to_lng"].stringValue) ?? 0
            let lat2 = Double(self.filterdata[indexPath.row]["from_lat"].stringValue ) ?? 0
            let long2 = Double(self.filterdata[indexPath.row]["from_lng"].stringValue ) ?? 0
                
            
            let distance = calculateDistance(lat1: lat1 , long1: long1 , lat2: lat2 , long2: long2 )
            cell.lblDistance.text = String(format: "%.2f km", distance)
          
            
        }
        else
        {
            let orderstatus = self.arrOrders[indexPath.row]["order_status"].intValue
            
             if orderstatus == 1 {
                           cell.lblStatus.text  = "NEW"
                           
                       } else if orderstatus == 2 {
                           cell.lblStatus.text  = "ACCEPTED"
                          
                       } else if orderstatus == 3 {
                           cell.lblStatus.text  = "OUT FOR DELIVERY"
                           
                       } else if orderstatus == 4 {
                           cell.lblStatus.text  = "DELIVERED"
                           
                       }
                       else if orderstatus == 5 {
                           cell.lblStatus.text  = "CANCELLED"
                           
                       }
                       else  {
                           cell.lblStatus.text  = "REFUND"
                       }
                       
            
           if self.arrOrders[indexPath.row]["order_type"].stringValue == "order"{
               
            cell.lblorderDelivery.text = "Order Delivery"
            let fullName = self.arrOrders[indexPath.row]["billing_address"].stringValue + " " + self.arrOrders[indexPath.row]["billing_city"].stringValue + " " + self.arrOrders[indexPath.row]["billing_state"].stringValue + " " + self.arrOrders[indexPath.row]["billing_zipcode"].stringValue
                           cell.lblAddressDetails.text = "\(fullName)"
//               cell.lblAddressDetails.text = self.arrOrders[indexPath.row]["billing_address"].stringValue
               cell.lblAddress.text = self.arrOrders[indexPath.row]["store_address"].stringValue
            cell.lblPrice.text = String(format: "$%@", self.arrOrders[indexPath.row]["delivery_charge"].stringValue)
               
           }
           else{
               cell.lblorderDelivery.text = "Parcel Delivery"
               cell.lblAddressDetails.text = self.arrOrders[indexPath.row]["to_pickup_address"].stringValue
               cell.lblAddress.text = self.arrOrders[indexPath.row]["from_pickup_address"].stringValue
            cell.lblPrice.text = String(format: "$%@", self.arrOrders[indexPath.row]["order_amount"].stringValue)
               
           }
            cell.orderNumber.text = String(format: "Order No. : %ld", self.arrOrders[indexPath.row]["order_id"].intValue)
            //order_amount
            
            
            
            let lat1 = Double(self.arrOrders[indexPath.row]["to_lat"].stringValue) ?? 0
            let long1 = Double(self.arrOrders[indexPath.row]["to_lng"].stringValue) ?? 0
            let lat2 = Double(self.arrOrders[indexPath.row]["from_lat"].stringValue ) ?? 0
            let long2 = Double(self.arrOrders[indexPath.row]["from_lng"].stringValue ) ?? 0
                
            
            let distance = calculateDistance(lat1: lat1 , long1: long1 , lat2: lat2 , long2: long2 )
            cell.lblDistance.text = String(format: "%.2f km", distance)
            

        }
        
        
        
        cell.view_Orderhistory.layer.cornerRadius = 6.0
        
        cell.lblorderDelivery.layer.cornerRadius = 5.0
        cell.lblorderDelivery.layer.masksToBounds=true
        
//        cell.btnAccept.layer.cornerRadius = cell.btnAccept.frame.height/2
//        cell.btnAccept.layer.masksToBounds=true
//
//        cell.btnAccept.addTarget(self, action: #selector(cellAcceptClickAction(_:)), for: .touchUpInside)
//        cell.btnReject.addTarget(self, action: #selector(cellRejectClickAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func cellAcceptClickAction(_ sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tableview_orderhistory)
        let indexPath = tableview_orderhistory.indexPathForRow(at:buttonPosition)
//        let cell = tableview_orderhistory.cellForRow(at: indexPath) as! UITableViewCell
//
//        guard let cell = sender.superview?.superview as? OrderHistoryCell else {            return        }
//
//        let indexPath = tableview_orderhistory.indexPath(for: cell)
        
//        print(indexPath?.row ?? 0)
        
        let index = indexPath?.row ?? 0
        
        let orderNumber = (issearching == true) ? filterdata[index]["order_id"].stringValue : arrOrders[index]["order_id"].stringValue
        
       

        
       
        
        orderHistoryAPI()
        
    }
    
    @objc func cellRejectClickAction(_ sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tableview_orderhistory)
        let indexPath = tableview_orderhistory.indexPathForRow(at:buttonPosition)

//        print(indexPath?.row ?? 0)
        
        let index = indexPath?.row ?? 0
        
        let orderNumber = (issearching == true) ? filterdata[index]["order_id"].stringValue : arrOrders[index]["order_id"].stringValue
        
        
        
        orderHistoryAPI()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath.row)
        
        var orderType = String()
        
        if issearching {
            orderID = self.filterdata[indexPath.row]["order_id"].intValue
            orderStatus = self.filterdata[indexPath.row]["order_status"].intValue
            orderType = self.filterdata[indexPath.row]["order_type"].stringValue
            name = self.filterdata[indexPath.row]["billing_name"].stringValue
            address = self.filterdata[indexPath.row]["billing_address"].stringValue
            phone = self.filterdata[indexPath.row]["billing_number"].stringValue
            email = self.filterdata[indexPath.row]["billing_email"].stringValue
            amount = self.filterdata[indexPath.row]["order_amount"].stringValue
            city = self.filterdata[indexPath.row]["billing_city"].stringValue
            State = self.filterdata[indexPath.row]["billing_state"].stringValue
            Zipcode = self.filterdata[indexPath.row]["billing_zipcode"].stringValue
            date = self.filterdata[indexPath.row]["order_date"].stringValue
            
            
        } else {
            orderID = self.arrOrders[indexPath.row]["order_id"].intValue
            orderStatus = self.arrOrders[indexPath.row]["order_status"].intValue
            orderType = self.arrOrders[indexPath.row]["order_type"].stringValue
            name = self.arrOrders[indexPath.row]["billing_name"].stringValue
            address = self.arrOrders[indexPath.row]["billing_address"].stringValue
            phone = self.arrOrders[indexPath.row]["billing_number"].stringValue
            email = self.arrOrders[indexPath.row]["billing_email"].stringValue
            amount = self.arrOrders[indexPath.row]["order_amount"].stringValue
            city = self.arrOrders[indexPath.row]["billing_city"].stringValue
            State = self.arrOrders[indexPath.row]["billing_state"].stringValue
            Zipcode = self.arrOrders[indexPath.row]["billing_zipcode"].stringValue
            date = self.arrOrders[indexPath.row]["order_date"].stringValue
        }
        
//        print(arrOrders[indexPath.row])
        
        
        
        let defaults = UserDefaults.standard
        defaults.set(name, forKey: "name")
        defaults.set(address, forKey: "address")
        defaults.set(phone, forKey: "phone")
        defaults.set(email, forKey: "email")
        defaults.set(orderID, forKey: "orderID")
        defaults.set(amount, forKey: "amount")
        defaults.set(city, forKey: "city")
        defaults.set(State, forKey: "state")
        defaults.set(Zipcode, forKey: "zipcode")
        defaults.set(date, forKey: "date")
        
//        print("order id is \(orderID) and orderStatus is \(orderStatus)")

        if orderStatus == 2 || orderStatus == 3 {
            
            let menu = self.storyboard?.instantiateViewController(withIdentifier: "ParcelAssignedVC") as! ParcelAssignedVC
            menu.orderId = orderID
            menu.orderStatus = orderStatus
            menu.orderType = orderType
            
            self.navigationController?.pushViewController(menu, animated: true)
            
           
            
        }else if orderStatus == 4 {
            
            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Order is already delivered"
                , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
//            print("Already Delivered")
            
        }else if orderStatus == 5 {
            
            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Order is Cancelled"
                , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
//            print("Cancelled.....")
            
        }else{
            
        }
        
    }
    
    //MARK: Actions
    
    
    
    @IBAction func leftVCBtnAction(_ sender: UIBarButtonItem) {
    self.sideMenuViewController?.presentLeftMenuViewController()
        
    }
   
    func calculateDistance(lat1: Double,long1:Double,lat2:Double,long2:Double)->Double{
    //My location
    let myLocation = CLLocation(latitude: lat1, longitude: long1)

    //My buddy's location
    let myBuddysLocation = CLLocation(latitude: lat2, longitude: long2)

    //Measuring my distance to my buddy's (in km)
    let distance = myLocation.distance(from: myBuddysLocation) / 1000
        return distance
    }
    
    func orderHistoryAPI() {
        
        if Reachability.isConnectedToNetwork() {
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            
            let apiOrderHistoryURL = "\(GlobalURL.baseURL1)\(GlobalURL.orderHistoryURL)"
//            print(apiOrderHistoryURL)
            let header = ["Authorization": "Bearer "+token]
            
//            print(header)
            
//            MBProgressHUD.showHUDMessage(message: "Getting order for you...", PPview: self.view)
            
            Alamofire.request(apiOrderHistoryURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
                MBProgressHUD.hideHUD()
                MBProgressHUD.hide(for: self.view, animated: true)
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        self.refreshControl.endRefreshing()
                        
                        self.arrOrders = swiftyJsonVar["result"]
                        
//                        print(self.arrOrders)
                        
//                        self.arrOrders = []
                        self.tableview_orderhistory.reloadData()
                        
                        
                        
                        
                       
                        
                        
                    } else {
                        UtilityMethods.hideIndicator()
                        
//                        self.bottomView.isHidden = true
                        self.tableview_orderhistory.isHidden = true
                        self.emptyView.isHidden = false
                        self.emptyImageView.image = UIImage(named: "empty_order_list")
                        self.emptyImageView.isHidden = false
                        self.emptyMessageLabel.isHidden = false
                        self.emptyLabel.isHidden = false
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
    
//}



//extension OrderHistoryVC {
    
    //MARK: ==============Get driver profile Api=============================
     func getDriverProfile() {
        
            let apiGetDriverProfile = "\(GlobalURL.baseURL)\(GlobalURL.driverProfileURL)"
//            print(apiGetDriverProfile)
           
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let header = ["Authorization": "Bearer "+token]

        
//            UtilityMethods.showIndicator(withMessage: "Getting order for you...")
        MBProgressHUD.showHUDMessage(message: "Getting order for you...", PPview: self.view)
            
            Alamofire.request(apiGetDriverProfile, method: .post, parameters: nil, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
//                MBProgressHUD.hideHUD()
                 
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        
                        self.driverStatus = swiftyJsonVar["result"]["ready_for_pickup"].stringValue
                               
                        
                        if swiftyJsonVar["result"]["work_permit_status"].intValue == 0 && swiftyJsonVar["result"]["police_clearance_status"].intValue == 0 && swiftyJsonVar["result"]["insurance_status"].intValue == 0 && swiftyJsonVar["result"]["license_status"].intValue == 0 {
                            
                            MBProgressHUD.hideHUD()
                            
                            self.bottomView.isHidden = true

                            self.documentMessage.isHidden = false
                            self.emptyView.isHidden = false
                            

                            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DocumentPopupVC") as! DocumentPopupVC
                            
                            popvc.modalPresentationStyle = .overCurrentContext
                            
                            self.present(popvc, animated: true, completion: nil)
                            
                            

                            
                        }else if swiftyJsonVar["result"]["driver_license"].stringValue == "" && swiftyJsonVar["result"]["work_permit_file"].stringValue == "" || swiftyJsonVar["result"]["police_clearance_file"].stringValue == "" || swiftyJsonVar["result"]["insurance_file"].stringValue == ""  {
                            
                            MBProgressHUD.hideHUD()
                            
                            self.bottomView.isHidden = true
                            self.emptyView.isHidden = false
                            self.documentMessage.isHidden = false
                            

                            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubmitDocumentPopupVC") as! SubmitDocumentPopupVC
                            
                            popvc.modalPresentationStyle = .overCurrentContext
                            
                            self.present(popvc, animated: true, completion: nil)
                            
                            
                           
                            
                        }
                        else{
                            
//                            if self.driverStatus == "1"{
//                                self.statusButton.setTitle("Offline", for: .normal)
//                                self.messageLabel.text="You are online"
//                                self.orderHistoryAPI()
//                            }else{
//                                self.statusButton.setTitle("Online", for: .normal)
//                                self.messageLabel.text="You are offline"
//                                self.arrOrders=[]
//                                self.tableview_orderhistory.reloadData()
//                                self.emptyView.isHidden = false
//
//                            }
                            
                            
                            self.online_OfflineMethod()
                            
                        }

                        
                        
                    } else {
                        
                        MBProgressHUD.hideHUD()
                        UtilityMethods.hideIndicator()
                        
//                        print(swiftyJsonVar["error"])
                    }
                }
            
        }
        
    }
    
    
    @IBAction func popupAcceeptBtnAction(_ sender: Any) {
        
        
        
      
        
        orderHistoryAPI()
        
    }
    
    
    @IBAction func popupRejectBtnAction(_ sender: Any) {
        
        
        
        orderHistoryAPI()
    }
    
    @IBAction func online_OfflineAction(_ sender: Any){
        
        if self.driverStatus == "1"{
            driverStatus="0"
         }else{
            driverStatus="1"
        }
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        online_OfflineMethod()
    }
    
    func online_OfflineMethod(){
        
            let apiChangeStatus = "\(GlobalURL.baseURL1)\(GlobalURL.driverStatus)"
//            print(apiChangeStatus)
            
            
            
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let header = ["Authorization": "Bearer "+token]
        
        let param:[String:Any]=["is_ready":self.driverStatus,"cur_lat":"\(latitudeVal)","cur_lng":
            "\(longitudeVal)"]
        
//          print(param)
        
        
            Alamofire.request(apiChangeStatus, method: .post, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON { response in
                
//                MBProgressHUD.hideHUD()
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        
                        self.driverStatus = swiftyJsonVar["result"]["ready_for_pickup"].stringValue
                        
                       
                        if self.driverStatus == "1"{
                            self.statusButton.setTitle("Offline", for: .normal)
                            self.messageLabel.text="You are online"
                            self.orderHistoryAPI()
                        }else{
                            MBProgressHUD.hideHUD()
                            self.statusButton.setTitle("Online", for: .normal)
                            self.messageLabel.text="You are offline"
                            self.arrOrders=[]
                            self.tableview_orderhistory.reloadData()
                            self.emptyView.isHidden = false
                            self.emptyImageView.isHidden = false
                            
                        }
                        
                        
                    } else {
                        
                        MBProgressHUD.hideHUD()
                        UtilityMethods.hideIndicator()
                        
                        self.bottomView.isHidden = true
                        self.emptyView.isHidden = false
                        self.documentMessage.isHidden = false

                        
                        
                        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DocumentPopupVC") as! DocumentPopupVC
                        
                        popvc.modalPresentationStyle = .overCurrentContext
                        
                        self.present(popvc, animated: true, completion: nil)
                        
                        
//                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
//                            , preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
               else {
                   UtilityMethods.hideIndicator()
                   MBProgressHUD.hideHUD()
                   
               }
            }
       
    }
    
    
    func getLastNotification(){
            
                let apiNotification = "\(GlobalURL.baseURL1)\(GlobalURL.getLastNotification)"
   
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            let header = ["Authorization": "Bearer "+token]

    
                Alamofire.request(apiNotification, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
    
                    if (response.result.value != nil){
                        if let swiftyJsonVar = response.result.value as? [String:Any]{
                            
                        
                        if let error = swiftyJsonVar["error"] as? Bool{
                            if error == false{
                            
                            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickupRequestVC") as! PickupRequestVC
                            
                            popvc.popupDict = swiftyJsonVar["result"] as! [String:Any]
                            
                            popvc.modalPresentationStyle = .overCurrentContext
                            
                            self.present(popvc, animated: true, completion: nil)
                            
                            
                        }
                            else{
                                
  
                            }
                        }
                        }
                            
                        } else {
                            
                            MBProgressHUD.hideHUD()
                            UtilityMethods.hideIndicator()
                            
    
                        }
                    }
                
            }
    
    
   
  
    
}
