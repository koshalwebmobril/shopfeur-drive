//
//  SignupVCs.swift
//  Shopfeur Users
//
//  Created by deepkohli on 01/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import YPImagePicker
import Photos
import Alamofire
import SwiftyJSON
import CountryPickerView

 

class SignupVCs: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,CountryPickerViewDelegate, CountryPickerViewDataSource {
    
//    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
//        return "Select a Country"
//    }
        
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {

        return .navigationBar
    }
    
    @IBOutlet weak var calendarImgView: UIImageView!
    
    @IBOutlet weak var contryLabel: UILabel!
    @IBOutlet weak var flagImgView: UIImageView!
    // Age of 18.
    let MINIMUM_AGE: Date = Calendar.current.date(byAdding: .year, value: -18, to: Date())!
    
    // Age of 50.
    let MAXIMUM_AGE: Date = Calendar.current.date(byAdding: .year, value: -50, to: Date())!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var presenter: SignupViewToPresenterProtocol?
    var window: UIWindow?
    let datePicker = UIDatePicker()
    var data = Data()
    
    var imgData = Data()
    var imagePicker = UIImagePickerController()
    
    var isaccepted = String()
    var acceptBtnVariable = String()
    
    var logoImgView : UIImageView!
    var countryName,codelabel : UILabel!
    
    var isProfileUpload : Bool = false
    
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_phonenumber: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_passwordBackground: UITextField!
    @IBOutlet weak var txt_Password: CustomTextField!
    @IBOutlet weak var txt_ConfirmPasswordBackground: UITextField!
    @IBOutlet weak var txt_confirmPassword: CustomTextField!
    @IBOutlet weak var txt_gender: UITextField!
    @IBOutlet weak var txt_date: UITextField!
    @IBOutlet weak var butt_signup: UIButton!
    @IBOutlet weak var lbl_signin: UILabel!
    @IBOutlet weak var buttonAcceptTerms: UIButton!
    @IBOutlet weak var labelTerms: UILabel!
    @IBOutlet weak var lbl_gotoTerms: UILabel!
    var countryCode :String = "+1"
    var genderStr : String = "0"
    
    @IBOutlet weak var countryListView: CountryPickerView!
    
    @IBOutlet weak var paddingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        txt_Password.delegate = self
        txt_confirmPassword.delegate = self
        
        labelTerms.text = "Accept "
//        lbl_gotoTerms.attributedText = NSAttributedString(string: "Terms & Conditions", attributes:
//            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        lbl_gotoTerms.text = "Terms & Conditions"
        
        // Do any additional setup after loading the view.
        img_Profile.layer.cornerRadius = img_Profile.frame.height/2
        img_Profile.layer.borderColor = UIColor.white.cgColor
        img_Profile.layer.shadowColor = UIColor.darkGray.cgColor
        img_Profile.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        img_Profile.layer.shadowRadius = 25.0
        img_Profile.layer.shadowOpacity = 0.9
        img_Profile.layer.borderWidth = 1.0
        txt_name.layer.cornerRadius = txt_name.frame.height/2
        txt_name.setLeftPaddingPoints(20)
        txt_name.setRightPaddingPoints(10)
        txt_name.delegate = self
        
        txt_phonenumber.layer.cornerRadius = txt_phonenumber.frame.height/2
         txt_phonenumber.setRightPaddingPoints(10)


        countryListView.delegate = self
        countryListView.dataSource = self
        txt_phonenumber.leftView = paddingView
        txt_phonenumber.leftViewMode = .always
    
        
        let country = countryListView.selectedCountry
 
        flagImgView.image = country.flag
            
            contryLabel.text = "\((country.code))  \(country.phoneCode)"
            countryCode = "\(country.phoneCode)"
        
        countryListView.countryDetailsLabel.font = UIFont.systemFont(ofSize: 15.0)
        countryListView.countryDetailsLabel.textColor = .white
     
       
        
        
        txt_passwordBackground.layer.cornerRadius = txt_passwordBackground.frame.height/2
        txt_Password.setLeftPaddingPoints(20)
        txt_Password.setRightPaddingPoints(10)
        txt_Password.delegate = self
        
        txt_ConfirmPasswordBackground.layer.cornerRadius = txt_ConfirmPasswordBackground.frame.height/2
        txt_confirmPassword.setLeftPaddingPoints(20)
        txt_confirmPassword.setRightPaddingPoints(10)
        txt_confirmPassword.delegate = self
        
        txt_email.layer.cornerRadius = txt_email.frame.height/2
        txt_email.setLeftPaddingPoints(20)
        txt_email.setRightPaddingPoints(10)
        txt_date.layer.cornerRadius = txt_date.frame.height/2
        txt_date.setLeftPaddingPoints(20)
        txt_date.setRightPaddingPoints(10)
        txt_gender.layer.cornerRadius = txt_gender.frame.height/2
        txt_gender.setLeftPaddingPoints(20)
        txt_gender.setRightPaddingPoints(10)
        butt_signup.layer.cornerRadius = butt_signup.frame.height/2
        self.lbl_signin.attributedText = concatAttributedString(firstString: AppConstants.Alreadyhaveaccount, secondString: AppConstants.signin)
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignupVCs.tapFunctionGender))
        txt_gender.isUserInteractionEnabled = true
        txt_gender.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(SignupVCs.gotoTerms))
        lbl_gotoTerms.isUserInteractionEnabled = true
        lbl_gotoTerms.addGestureRecognizer(tap1)
        
        showDatePicker()
        
        let tapCalGesture = UITapGestureRecognizer(target: self, action: #selector(showCalender(_:)))
        calendarImgView.isUserInteractionEnabled = true
        
        calendarImgView.addGestureRecognizer(tapCalGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showCountry(_:)))
        paddingView.isUserInteractionEnabled = true
        paddingView.addGestureRecognizer(tapGesture)
    }
    
    @objc func showCalender(_ sender:UITapGestureRecognizer){
        
        txt_date.becomeFirstResponder()
        
    }
    
    @objc func showCountry(_ sender:UITapGestureRecognizer){
        
        countryListView.showCountriesList(from: self)
        
    }
    
    @IBAction func showCountryList(_ sender: Any) {
        
        countryListView.showCountriesList(from: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @objc func gotoTerms(sender:UITapGestureRecognizer) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        vc.labeltitle = "Terms & Conditions"
        vc.modalPresentationStyle = .fullScreen
        vc.isfromSignup = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func tapFunctionGender(sender:UITapGestureRecognizer) {
        
        let actionSheetController = UIAlertController(title: "Select your gender", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let maleAction = UIAlertAction(title: "Male", style: UIAlertAction.Style.default) { (action) -> Void in
            self.txt_gender.text = "Male"
            self.genderStr = "1"
        }
        let femaleAction = UIAlertAction(title: "Female", style: UIAlertAction.Style.default) { (action) -> Void in
            self.txt_gender.text = "Female"
            self.genderStr = "2"
        }
        let otherAction = UIAlertAction(title: "Other", style: UIAlertAction.Style.default) { (action) -> Void in
            self.txt_gender.text = "Other"
            self.genderStr = "3"
        }
        
        actionSheetController.addAction(maleAction)
        actionSheetController.addAction(femaleAction)
        actionSheetController.addAction(otherAction)
        present(actionSheetController, animated: true, completion: nil)
        
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: 800, width: 20, height: 0)
            popoverController.permittedArrowDirections = []
        }
    }
    
    
//    @objc func countryListShow(_ sender:UITapGestureRecognizer){
//
//        countryList.showCountriesList(from: self)
//
//        print("Tap........")
//    }
   
    func showDatePicker(){
        //Formate Date
        txt_date.tintColor = .clear
        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txt_date.inputAccessoryView = toolbar
        txt_date.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txt_date.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func acceptTermsButton(_ sender: Any) {
        if self.isaccepted == "selected"
        {
            buttonAcceptTerms.setImage(UIImage(named: "NotAccepted"), for: UIControl.State.normal)
            self.isaccepted = ""
            acceptBtnVariable = ""
        }
        else
        {
            buttonAcceptTerms.setImage(UIImage(named: "Accepted"), for: UIControl.State.normal)
            self.isaccepted = "selected"
            acceptBtnVariable = "1"
        }
        
    }
    
    //MARK: Action
    
    @IBAction func action_signup(_ sender: Any) {
        
        self.view.endEditing(true)
        if isProfileUpload == false{
            
            statusBarErrorAlert(msg: "Please upload profile image")
            
        }
        else if txt_name.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.Emptyname)
        } else if txt_phonenumber.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.EmptyMobileNumber)
        } else if !txt_phonenumber.text!.isValidPhoneNumber() {
            statusBarErrorAlert(msg: AppConstants.InvalidMobileNumber)

        }
        else if txt_Password.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.EmptyPassword)
        } else if txt_Password.text!.count < 6 {
            statusBarErrorAlert(msg: "Password length should be atleast 6 character")

        } else if txt_confirmPassword.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.EmptyConfirmPassword)
        } else if txt_Password.text! != txt_confirmPassword.text! {
            statusBarErrorAlert(msg: AppConstants.PasswordnotMatch)
        } else if acceptBtnVariable == "" {
            
            statusBarErrorAlert(msg: AppConstants.acceptTerms)
        } else {
            data = self.img_Profile.image!.jpegData(compressionQuality: 0.5)!
            UserDefaults.standard.set(txt_phonenumber.text!, forKey: "MOBILENUMBER")
            UserDefaults.standard.set(txt_name.text!, forKey: "NAME")
            UserDefaults.standard.synchronize()
//            presenter?.callSignupAPI(name: txt_name.text!.removingWhitespacesAndNewlines, mobileNumber: txt_phonenumber.text!.removingWhitespacesAndNewlines,password: txt_Password.text!.removingWhitespacesAndNewlines,imageData:data)
            signUp()
        }
        
    }
    
    func validateAge(birthDate: Date) -> Bool {
        var isValid: Bool = true;
        
        if birthDate < MAXIMUM_AGE || birthDate > MINIMUM_AGE {
            isValid = false;
        }
        
        return isValid;
    }
    
    
    @IBAction func action_openGallery(_ sender: Any) {
        self.view.endEditing(true)
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = true
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .cappedTo(size: 1024)
//        config.library.mediaType = .photo
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.startOnScreen = .library
//        config.screens = [.library, .photo,]
//        config.showsCrop = .none
//        config.wordings.libraryTitle = AppConstants.librarytitle
//        config.hidesStatusBar = true
//        config.hidesBottomBar = false
//        config.library.maxNumberOfItems = 1
//
//        let picker = YPImagePicker(configuration: config)
//        /* Single Photo implementation. */
//        picker.didFinishPicking { [unowned picker] items, _ in
//        self.img_Profile.image = items.singlePhoto?.image
//            picker.dismiss(animated: true, completion: nil)
//        }
//        present(picker, animated: true, completion: nil)
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        isProfileUpload = true
        
        self.img_Profile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        

        
        flagImgView.image = country.flag
        
        contryLabel.text = "\((country.code))  \(country.phoneCode)"
        countryCode = "\(country.phoneCode)"
    }
    
   
    @IBAction func action_PasswordShowHide(_ sender: UIButton) {
//        print(sender.tag)
        
        switch sender.tag {
        case 3:
             if PasswordShowHide(textfield: txt_Password) {
                txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
             } else {
                txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
            }
        default:
             if PasswordShowHide(textfield: txt_confirmPassword) {
                txt_confirmPassword.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
            } else {
                txt_confirmPassword.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        txt_Password.isSecureTextEntry = true
        
//        if txt_Password.text == "" {
//
//        } else if (txt_Password.isSecureTextEntry) {
//            txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
//        } else {
//            txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
//        }
    }
    
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if txt_Password.text == "" {
//
//        } else if (txt_Password.isSecureTextEntry) {
//            txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
//        } else {
//            txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
//        }
//        return true
//    }
    
    
    @IBAction func gotoSignin(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        //appDelegate.callSigninController()
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txt_phonenumber {
//            let maxLength = 15
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//        } else if textField == txt_Password {
//            let maxLength = 16
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//        } else {
//            return true
//        }
//    }
    
    
    
    
    func signUp()  {
        
        let Token = UserDefaults.standard.value(forKey: "fcm_token")  as? String ?? "asdfg"
        
        let apiRegisterURL = "\(GlobalURL.baseURL)\(GlobalURL.signUpURL)"
//        print(apiRegisterURL)
        
        imgData = (img_Profile.image!.jpegData(compressionQuality: 0.4))!
//        print(imgData)
       
        
        let newTodo: [String: Any] = ["first_name": "\(txt_name.text ?? "")", "mobile": "\(txt_phonenumber.text ?? "")", "email": "\(txt_email.text ?? "")", "password": "\(txt_Password.text ?? "")", "password_confirmation": "\(txt_confirmPassword.text ?? "")", "is_term_accept": "\(acceptBtnVariable)", "device_type": "\(deviceType)", "device_token": Token, "dob": "\(txt_date.text ?? "")", "gender": "\(self.genderStr )","country_code":"\(countryCode)"]
//        print("parameters are \(newTodo)")
        
       // UtilityMethods.showIndicator(withMessage: "Please wait...")
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.imgData, withName: "image",fileName: "profile.jpg", mimeType: "image/jpg")
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiRegisterURL) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
        
        
                    MBProgressHUD.hideHUD()
        

            if (response.result.value != nil){
              
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()
                    let responceJson = swiftyJsonVar["result"]
//                     print(responceJson)
                    let otp = responceJson["otp"].stringValue
                    UserDefaults.standard.set(otp, forKey: "OTP")
                    UserDefaults.standard.set(responceJson["token"].stringValue, forKey: "authToken")
                   
                    let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
                    if let view = storyboard.instantiateViewController(withIdentifier: "OtpScreenVCs") as? OtpScreenVCs {
                        view.comeFrom = "signup"
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                   
                } else {
                   UtilityMethods.hideIndicator()
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }

            case .failure(let encodingError):
                print(encodingError)
            }
    
        }
    }
    
    
    
}

extension SignupVCs: SignupPresenterToViewProtocol {
    
    func noInternet() {
        statusBarErrorAlert(msg: AppConstants.NoInternet)
    }
    
    func showAlert(title: String, msg: String) {
        showAlertDialog(title: title, description: msg)
    }
    
    func fetchSignupdata(dict: SignupEntities) {
        if dict.error! {
            statusBarErrorAlert(msg: dict.message!)
        } else {
            let otp = dict.result?.object(forKey: "otp") as? String ?? ""
            appDelegate.callVerifyOTPController()
            UserDefaults.standard.set(otp, forKey: "OTPP")
            UserDefaults.standard.synchronize()
        }
    }
}

