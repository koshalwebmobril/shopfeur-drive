//
//  AddVehicleDetailsVC.swift
//  Shopfeur Courier
//
//  Created by Sushant on 19/11/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import RangeSeekSlider
import YPImagePicker
import Alamofire
import SwiftyJSON
import DropDown
import Foundation


class AddVehicleDetailsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {
    
    var arrVehicleImage: [Data] = []
    var imgData1 = Data()
    var imgData2 = Data()
    var imgData3 = Data()
    var imgData4 = Data()
    var minVal = "20"
    var maxVal = "80"
//    var arrVehicleType : [String] = []
    var arrVehicleType = [JSON]()
    var vehicleType = String()
    
    var imagePicker = UIImagePickerController()
    var imgString = ""
    var isfromMenu = String()
    
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var vehicleImage1: UIImageView!
    @IBOutlet weak var vehicleImage2: UIImageView!
    @IBOutlet weak var vehicleImage3: UIImageView!
    @IBOutlet weak var vehicleImage4: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tfModelName: UITextField!
    @IBOutlet weak var tfModelYear: UITextField!
    @IBOutlet weak var tfModelType: UITextField!
    @IBOutlet weak var tfRegistrationNmbr: UITextField!
    
    
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var btn_Dropdown: UIButton!
    
    @IBOutlet weak var backButtonWidth: NSLayoutConstraint!
    
    var imagesDataArr: [Data] = [Data]()
    let vehicleDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
           
            self.vehicleDropDown
        
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
            self.getVehicleTypeAPI()
        
        if isfromMenu.isEmpty{
            backButtonWidth.constant = 0.0
            backButton.isHidden = true
        }
        
        imagePicker.delegate = self
        
        self.submitButton.layer.cornerRadius = self.submitButton.frame.height/2
        self.submitButton.layer.masksToBounds = true
        
        self.progressBar.layer.cornerRadius = 5.0
        self.progressBar.layer.masksToBounds = true
        self.progressBar.clipsToBounds = true
        
        tfModelName.backgroundColor = UIColor.white
        tfModelYear.backgroundColor = UIColor.white
        tfModelType.backgroundColor = UIColor.white
        tfRegistrationNmbr.backgroundColor = UIColor.white
        
        tfModelName.layer.cornerRadius = tfModelName.frame.height/6
        tfModelName.clipsToBounds = true
        tfModelName.layer.borderWidth = 2
        tfModelName.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        tfModelYear.layer.cornerRadius = tfModelYear.frame.height/6
        tfModelYear.clipsToBounds = true
        tfModelYear.layer.borderWidth = 2
        tfModelYear.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        tfModelType.layer.cornerRadius = tfModelType.frame.height/6
        tfModelType.clipsToBounds = true
        tfModelType.layer.borderWidth = 2
        tfModelType.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        tfRegistrationNmbr.layer.cornerRadius = tfRegistrationNmbr.frame.height/6
        tfRegistrationNmbr.clipsToBounds = true
        tfRegistrationNmbr.layer.borderWidth = 2
        tfRegistrationNmbr.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        tfModelName.setLeftPaddingPoints(10)
        tfModelName.setRightPaddingPoints(10)
        
        tfModelYear.setLeftPaddingPoints(10)
        tfModelYear.setRightPaddingPoints(10)
        
        tfModelType.setLeftPaddingPoints(10)
        tfModelType.setRightPaddingPoints(10)
        
        tfRegistrationNmbr.setLeftPaddingPoints(10)
        tfRegistrationNmbr.setRightPaddingPoints(10)
        
        tfModelName.delegate = self
        tfModelYear.delegate = self
        tfModelType.delegate = self
        tfRegistrationNmbr.delegate = self
        
        vehicleImage1.isUserInteractionEnabled = true
        vehicleImage2.isUserInteractionEnabled = true
        vehicleImage3.isUserInteractionEnabled = true
        vehicleImage4.isUserInteractionEnabled = true
        
        setup()
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(AddVehicleDetailsVC.tapFunction))
        vehicleImage1.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(AddVehicleDetailsVC.tapFunction1))
        vehicleImage2.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(AddVehicleDetailsVC.tapFunction2))
        vehicleImage3 .addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(AddVehicleDetailsVC.tapFunction3))
        vehicleImage4.addGestureRecognizer(tap4)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setBiketext(_:)), name: NSNotification.Name("setBike"), object: nil)
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func setBiketext(_ sender:Notification){
        
        tfModelType.text = sender.userInfo?["name"] as? String ?? ""
        vehicleType = sender.userInfo?["vehicle_type"] as! String  
        
        
        
    }
    
    @IBAction func dropDownAction(_ sender: Any) {
        
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VehiclePopupVC") as! VehiclePopupVC
        
        popvc.modalPresentationStyle = .overCurrentContext
        popvc.arrVehicle = self.arrVehicleType
        
        self.present(popvc, animated: true, completion: nil)
    }
    
   func setupVehicleDropDown() {
        
        
//        vehicleDropDown.anchorView = btn_Dropdown

        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
//        vehicleDropDown.bottomOffset = CGPoint(x: 0, y: btn_Dropdown.bounds.height)

        // You can also use localizationKeysDataSource instead. Check the docs.
//        vehicleDropDown.dataSource = self.arrVehicleType

        // Action triggered on selection
//        vehicleDropDown.selectionAction = { [weak self] (index, item) in
//            self?.btn_Dropdown.setTitle(item, for: .normal)
//            self?.tfModelType.text = item
//        }
//
//        vehicleDropDown.width = 200

     
    }
    
    
    
    
  
    
    
    private func setup() {
        rangeSlider.delegate = self
        
        
        rangeSlider.minValue = 0
        rangeSlider.maxValue = 100
        rangeSlider.selectedMinValue = 20
        rangeSlider.selectedMaxValue = 80
        rangeSlider.selectedHandleDiameterMultiplier = 2.0
        rangeSlider.colorBetweenHandles = #colorLiteral(red: 0.5076663494, green: 0.5975046754, blue: 0.9720574021, alpha: 1)
        rangeSlider.lineHeight = 5.0
        rangeSlider.numberFormatter.positiveSuffix = "KM"
    }
    
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        imgString = "0"
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tapFunction1(sender:UITapGestureRecognizer) {
        
        imgString = "1"
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func tapFunction2(sender:UITapGestureRecognizer) {
        imgString = "2"
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    @objc func tapFunction3(sender:UITapGestureRecognizer) {
        imgString = "3"
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        switch imgString {
        case "0":
            self.vehicleImage1.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.arrVehicleImage.append(self.imgData1)
        case "1":
            self.vehicleImage2.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.arrVehicleImage.append(self.imgData2)
        case "2":
            self.vehicleImage3.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.arrVehicleImage.append(self.imgData3)
        default:
            self.vehicleImage4.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.arrVehicleImage.append(self.imgData4)
        }
        
       
        dismiss(animated: true, completion: nil)
        
    }
    
//    @objc func tapFunction1(sender:UITapGestureRecognizer) {
//
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = true
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .cappedTo(size: 1024)
//        config.library.mediaType = .photo
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.startOnScreen = .library
//        config.screens = [.library, .photo,]
//        config.showsCrop = .none
//        config.wordings.libraryTitle = AppConstants.librarytitle
//        config.hidesStatusBar = true
//        config.hidesBottomBar = false
//        config.library.maxNumberOfItems = 1
//
//        let picker = YPImagePicker(configuration: config)
//        /* Single Photo implementation. */
//        picker.didFinishPicking { [unowned picker] items, _ in
//
//            self.vehicleImage1.image = items.singlePhoto?.image ?? UIImage(named: "addImage")
//
//            self.arrVehicleImage.append(self.imgData1)
//            picker.dismiss(animated: true, completion: nil)
//
//
//        }
//
//        //        picker.didFinishPicking { [unowned picker] items, cancelled in
//        //            if cancelled {
//        //                print("Picker was canceled")
//        //            }
//        //            picker.dismiss(animated: true, completion: nil)
//        //        }
//        present(picker, animated: true, completion: nil)
//
//    }
//
//    @objc func tapFunction2(sender:UITapGestureRecognizer) {
//
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = true
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .cappedTo(size: 1024)
//        config.library.mediaType = .photo
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.startOnScreen = .library
//        config.screens = [.library, .photo,]
//        config.showsCrop = .none
//        config.wordings.libraryTitle = AppConstants.librarytitle
//        config.hidesStatusBar = true
//        config.hidesBottomBar = false
//        config.library.maxNumberOfItems = 1
//
//        let picker = YPImagePicker(configuration: config)
//        /* Single Photo implementation. */
//        picker.didFinishPicking { [unowned picker] items, _ in
//
//            self.vehicleImage2.image = items.singlePhoto?.image ?? UIImage(named: "addImage")
//
//            self.arrVehicleImage.append(self.imgData2)
//            picker.dismiss(animated: true, completion: nil)
//
//
//        }
//
//        //        picker.didFinishPicking { [unowned picker] items, cancelled in
//        //            if cancelled {
//        //                print("Picker was canceled")
//        //            }
//        //            picker.dismiss(animated: true, completion: nil)
//        //        }
//        present(picker, animated: true, completion: nil)
//
//    }
//    @objc func tapFunction3(sender:UITapGestureRecognizer) {
//
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = true
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .cappedTo(size: 1024)
//        config.library.mediaType = .photo
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.startOnScreen = .library
//        config.screens = [.library, .photo,]
//        config.showsCrop = .none
//        config.wordings.libraryTitle = AppConstants.librarytitle
//        config.hidesStatusBar = true
//        config.hidesBottomBar = false
//        config.library.maxNumberOfItems = 1
//
//        let picker = YPImagePicker(configuration: config)
//        /* Single Photo implementation. */
//        picker.didFinishPicking { [unowned picker] items, _ in
//
//            self.vehicleImage3.image = items.singlePhoto?.image ?? UIImage(named: "addImage")
//
//            self.arrVehicleImage.append(self.imgData3)
//
//            print(self.arrVehicleImage)
//            picker.dismiss(animated: true, completion: nil)
//
//
//        }
//
//        //        picker.didFinishPicking { [unowned picker] items, cancelled in
//        //            if cancelled {
//        //                print("Picker was canceled")
//        //            }
//        //            picker.dismiss(animated: true, completion: nil)
//        //        }
//        present(picker, animated: true, completion: nil)
//
//    }
//    @objc func tapFunction4(sender:UITapGestureRecognizer) {
//
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = true
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .cappedTo(size: 1024)
//        config.library.mediaType = .photo
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.startOnScreen = .library
//        config.screens = [.library, .photo,]
//        config.showsCrop = .none
//        config.wordings.libraryTitle = AppConstants.librarytitle
//        config.hidesStatusBar = true
//        config.hidesBottomBar = false
//        config.library.maxNumberOfItems = 1
//
//        let picker = YPImagePicker(configuration: config)
//        /* Single Photo implementation. */
//        picker.didFinishPicking { [unowned picker] items, _ in
//
//            self.vehicleImage4.image = items.singlePhoto?.image ?? UIImage(named: "addImage")
//
//            self.arrVehicleImage.append(self.imgData4)
//            print(self.arrVehicleImage)
//            picker.dismiss(animated: true, completion: nil)
//
//
//
//        }
//
//        //        picker.didFinishPicking { [unowned picker] items, cancelled in
//        //            if cancelled {
//        //                print("Picker was canceled")
//        //            }
//        //            picker.dismiss(animated: true, completion: nil)
//        //        }
//        present(picker, animated: true, completion: nil)
//
//    }
    
    //MARK: Actions
    
    @IBAction func submitButtonAction(_ sender: Any) {
        if (tfModelName.text == "") {
             statusBarErrorAlert(msg: AppConstants.EnterModelName)
        } else if (tfModelType.text == "") {
            statusBarErrorAlert(msg: AppConstants.EnterType)
        } else if (tfModelYear.text == "") {
            statusBarErrorAlert(msg: AppConstants.EnterYear)
        } else if (tfRegistrationNmbr.text == "") {
            statusBarErrorAlert(msg: AppConstants.EnterRegistrationNo)
        } else if (arrVehicleImage.count < 2) {
            
//            print(imagesDataArr)
            statusBarErrorAlert(msg: AppConstants.SelectImages)
            
        } else {
            
            self.apiVehicleDetails()

        }
        
    }
    
    @IBAction func btn_VehicleTypeAction(_ sender: Any) {
        
        vehicleDropDown.show()
        
    }
    
    
    
    
    func apiVehicleDetails() {
        
        let apiVehicleDetailsURL = "\(GlobalURL.baseURL1)\(GlobalURL.addVehicleInfoURL)"
        
         self.imgData1 = self.vehicleImage1.image!.jpegData(compressionQuality: 0.4)!
         self.imgData2 = self.vehicleImage2.image!.jpegData(compressionQuality: 0.4)!
         self.imgData3 = self.vehicleImage3.image!.jpegData(compressionQuality: 0.4)!
         self.imgData4 = self.vehicleImage4.image!.jpegData(compressionQuality: 0.4)!
        
        let parameters = ["model_name": "\(tfModelName.text ?? "")", "model_year": "\(tfModelYear.text ?? "")", "registration_number": "\(tfRegistrationNmbr.text ?? "")", "vehicle_type": "\(vehicleType)", "min_range": minVal , "max_range": maxVal]
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        
        
        
        let url = apiVehicleDetailsURL
//       ÷\ print(url)
//        print(parameters)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+token ,
            "Content-type": "multipart/form-data"
        ]
//        print(headers)
        
        imagesDataArr.append(imgData1)
        imagesDataArr.append(imgData2)
        imagesDataArr.append(imgData3)
        imagesDataArr.append(imgData4)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
            
            let count = self.imagesDataArr.count
            
            
            
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            for index in 0..<count {
                
                multipartFormData.append(self.imagesDataArr[index], withName: "vehicle_image[]", fileName: "image\(index).png", mimeType: "image/png")
                
            }
            

//            print("check Multipart \(multipartFormData)" )
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    MBProgressHUD.hideHUD()
                    UtilityMethods.hideIndicator()
//                    print("Succesfully uploaded")
//                    print(parameters)
                    
                   
                    
//                    print("Done")
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
                            
                            let responceJson = swiftyJsonVar["result"]
//                            print(responceJson)
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
                                action in
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "OtherDocumentsVC") as! OtherDocumentsVC
                                vc.modalPresentationStyle = .fullScreen
                                vc.isfromMenu = self.isfromMenu
                                self.present(vc, animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            case .failure(let error):
                
                UtilityMethods.hideIndicator()
//                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    
    @IBAction func addBackButtonAction(_ sender: Any) {
        
        if isfromMenu.isEmpty {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getVehicleTypeAPI() {
        
        if Reachability.isConnectedToNetwork() {
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            
            let apiGetVehicleTypeURL = "\(GlobalURL.baseURL1)\(GlobalURL.getVehicleTypeURL)"
//            print(apiGetVehicleTypeURL)
            let header = ["Authorization": "Bearer "+token]
            
//            print(header)
//            UtilityMethods.showIndicator(withMessage: "Please wait...")
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            Alamofire.request(apiGetVehicleTypeURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
                MBProgressHUD.hideHUD()
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        let responceJson = swiftyJsonVar["result"]
                        
                        self.arrVehicleType.removeAll()
//                        print(responceJson.count)
                        
                        self.arrVehicleType = swiftyJsonVar["result"].arrayValue

//                        for index in 0..<responceJson.count{
//                            self.vehicleType = responceJson[index]["name"].stringValue
//                            self.arrVehicleType.append(self.vehicleType)
//                        }
                       

//                        print(self.arrVehicleType)

//                        self.setupVehicleDropDown()
                        
                    } else {
                        UtilityMethods.hideIndicator()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
    
    
    
}

extension AddVehicleDetailsVC: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
       
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        formatter.numberStyle = .decimal
        minVal = formatter.string(from: minValue as NSNumber) ?? "20"
        maxVal = formatter.string(from: maxValue as NSNumber) ?? "80"
       
  
//        print("Standard slider updated. Min Value: \(minVal) Max Value: \(maxVal)")
    
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
//        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
//        print("did end touches")
    }
}
