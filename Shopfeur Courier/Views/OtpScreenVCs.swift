//
//  OtpScreenVCs.swift
//  Shopfeur Users
//
//  Created by deepkohli on 01/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OtpScreenVCs: UIViewController,UITextFieldDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var presenter: OTPScreenViewToPresenterProtocol?
    var window: UIWindow?
    var comeFrom = ""
    var otp = String()
    var custId = Int()
    var mobile = String()
    
    @IBOutlet weak var txt_otp: UITextField!
    @IBOutlet weak var butt_Submit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        txt_otp.layer.cornerRadius = txt_otp.frame.height/2
        txt_otp.delegate = self
        butt_Submit.layer.cornerRadius = butt_Submit.frame.height/2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if comeFrom == "signup" {
            otp = UserDefaults.standard.value(forKey: "OTP") as? String ?? ""
        }else if comeFrom == "signin" {
            otp = UserDefaults.standard.value(forKey: "OTP") as? String ?? ""
        }
        
        else {
            otp = UserDefaults.standard.value(forKey: "OTPP") as? String ?? ""
        }
        
//        print(otp)
        
        if otp == "" {
            
        } else {
//            txt_otp.text = otp
        }
        UserDefaults.standard.removeObject(forKey: "OTPP")
        UserDefaults.standard.removeObject(forKey: "OTP")
    }
    
    @IBAction func action_resendcode(_ sender: Any) {
        otpByNumber()
      }
    
    @IBAction func action_Submit(_ sender: Any) {
        
        self.view.endEditing(true)
        if txt_otp.text!.isEmpty
        {
            statusBarErrorAlert(msg: AppConstants.EmptyOTP)
        }
        else if  txt_otp.text!.count != 6
               {
                   statusBarErrorAlert(msg: "Please enter valid OTP")
               }
        
        else {
//            presenter?.callOTPVerifyAPI(OTP: txt_otp.text!.removingWhitespacesAndNewlines)
            submitOTP()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_otp {
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }  else {
            return true
        }
    }
    
    
    @IBAction func action_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func submitOTP(){
        
        let apiVerifyOtpURL = "\(GlobalURL.baseURL)\(GlobalURL.verifyOtpURL)"
//        print(apiVerifyOtpURL)
        if comeFrom == "signup" {
             mobile = UserDefaults.standard.object(forKey: "MOBILENUMBER") as! String
        }else if comeFrom == "signin" {
             mobile = UserDefaults.standard.object(forKey: "MOBILE") as! String
        }
        else {
            mobile = UserDefaults.standard.object(forKey: "numberforpassword") as! String
        }
       
        
        let newTodo: [String: Any] = ["mobile": mobile, "otp": "\(txt_otp.text ?? "")"]
//        print(newTodo)
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(apiVerifyOtpURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            MBProgressHUD.hideHUD()
            
            if (response.result.value != nil){
//                print(response)
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()

                    let responceJson = swiftyJsonVar["result"]
//                    print(responceJson)
                    UserDefaults.standard.set(swiftyJsonVar["result"]["otp"].stringValue, forKey: "otpforpassword")
                    UserDefaults.standard.set(swiftyJsonVar["result"]["mobile"].stringValue, forKey: "numberforpassword")
                    
                    
                     
                    
                    if self.comeFrom == "signup" {
                        UserDefaults.standard.set(responceJson["id"].intValue, forKey: "user_Id")
                        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
                        if let view = storyboard.instantiateViewController(withIdentifier: "WelcomeVC") as? WelcomeVC {
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }else if self.comeFrom == "signin"{
                        UserDefaults.standard.set(responceJson["id"].intValue, forKey: "user_Id")
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                        
                         vc.modalPresentationStyle = .fullScreen
                         self.present(vc, animated: true, completion: nil)
                    }
                    
                    
                    else {
                        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
                        if let view = storyboard.instantiateViewController(withIdentifier: "CreateNewPasswordVC") as? CreateNewPasswordVC {
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }
                    
                    
                } else {
                    UtilityMethods.hideIndicator()
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func otpByNumber() {
        
        
        let apiOtpByNumberURL = "\(GlobalURL.baseURL)\(GlobalURL.forgotPasswordURL)"
//        print(apiOtpByNumberURL)
        if comeFrom == "signup" {
             mobile = UserDefaults.standard.object(forKey: "MOBILENUMBER") as! String
        }else if comeFrom == "signin" {
             mobile = UserDefaults.standard.object(forKey: "MOBILE") as! String
        }
        else {
            mobile = UserDefaults.standard.object(forKey: "numberforpassword") as! String
        }
        
        let newTodo: [String: Any] = ["mobile": mobile]
//        print(newTodo)
        
        //UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(apiOtpByNumberURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            
            MBProgressHUD.hideHUD()
            
            if (response.result.value != nil){
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()
                    
                    let otp = swiftyJsonVar["result"].stringValue
//                    print(otp)
                    
//                    self.txt_otp.text = otp
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Otp has been sent to your mobile."
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {action in
//                        self.txt_otp.text = otp
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    UtilityMethods.hideIndicator()
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    
}

extension OtpScreenVCs: OTPScreenPresenterToViewProtocol {
    
    func fetchOTPdata(dict: OTPScreenEntities) {
        if dict.error! {
            statusBarErrorAlert(msg: dict.message!)
        } else {
            appDelegate.callRootViewController()
            UserDefaults.standard.set(dict.result?.object(forKey: "image"), forKey: "PROFILEDATA")
            UserDefaults.standard.set(true, forKey: AppConstants.LoginComplete)
            UserDefaults.standard.synchronize()
            statusBarSuccessAlert(msg: dict.message!)
        }
    }
    
    func noInternet() {
        statusBarErrorAlert(msg: AppConstants.NoInternet)
    }
    
    func showAlert(title: String, msg: String) {
        showAlertDialog(title: title, description: msg)
    }
    
}
