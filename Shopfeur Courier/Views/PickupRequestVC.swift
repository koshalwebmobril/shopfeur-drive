//
//  PickupRequestVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 18/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class PickupRequestVC: UIViewController {
    
    var orderId = Int()
    var orderStatus = Int()
    var acceptRejectStatus = Int()
    var isfromNotification = String()
    
    var popupDict = [String:Any]()
    
    @IBOutlet weak var orderDelivery: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var orderType: UILabel!
    
    @IBOutlet weak var lblFare: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPickupAddress: UILabel!
    @IBOutlet weak var lblDropAddress: UILabel!
   
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var bgView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        popupView.layer.cornerRadius = 2
        popupView.layer.borderColor = UIColor.gray.cgColor
        popupView.layer.borderWidth = 1.0
        popupView.layer.masksToBounds = true
        
        backView.layer.cornerRadius = 10
        backView.layer.shadowColor = UIColor.gray.cgColor
        backView.layer.shadowOpacity = 1
        backView.layer.shadowOffset = CGSize.zero
        backView.layer.shadowRadius = 2.5
        
        orderDelivery.layer.masksToBounds = true
        orderDelivery.layer.cornerRadius = 10
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(PickupRequestVC.tap))
        bgView.addGestureRecognizer(tap)

//        print("Popup Data : \(popupDict)")
        
        
        if let order_Type = popupDict["order_type"] as? String{
        if order_Type == "order"{
            orderDelivery.text = "Order Delivery"
            orderType.text = "New Order Request"
            
        }else{
            orderDelivery.text = "Parcel Delivery"
            orderType.text = "New Parcel Request"
        }
        }
        
        lblPickupAddress.text = popupDict["from_pickup_address"] as? String ?? "-"
//        lblDropAddress.text = popupDict["from_pickup_address"] as? String ?? "-"
//        lblFare.text = popupDict["data_id"] as? String ?? "0.00"
        
        var lat1 = Double()
        var long1 = Double()
        var lat2 = Double()
        var long2 = Double()
        
        if let lat =  popupDict["pickup_lat"] as? Double{
            lat1 = lat
        }
        if let long = popupDict["pickup_lng"] as? Double{
            long1 = long
        }
        if let latitute = popupDict["drop_lat"] as? Double{
            lat2 = latitute
            
        }
        if let longitude = popupDict["drop_lng"] as? Double{
            long2 = longitude
        }
        
        let distance = calculateDistance(lat1: lat1, long1: long1, lat2: lat2, long2: long2)
        
        lblDistance.text = String(format: "%.2f km", distance)
        
        orderId = popupDict["data_id"] as! Int
        
    }
    
   
    func calculateDistance(lat1: Double,long1:Double,lat2:Double,long2:Double)->Double{
    //My location
    let myLocation = CLLocation(latitude: lat1, longitude: long1)

    //My buddy's location
    let myBuddysLocation = CLLocation(latitude: lat2, longitude: long2)

    //Measuring my distance to my buddy's (in km)
    let distance = myLocation.distance(from: myBuddysLocation) / 1000
        return distance
    }
   
    @objc func tap(_sender : UITapGestureRecognizer) {
        
//        self.view.removeFromSuperview()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func rejectAction(_ sender: Any) {
        acceptRejectStatus = 0
        
        reject()

    }
    
    @IBAction func acceptAction(_ sender: Any) {
        
        acceptRejectStatus = 1
        acceptRejectAPI()

        
    }
    
    
   
    
    func acceptRejectAPI() {
        
        if Reachability.isConnectedToNetwork() {
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            
            let apiAcceptRejectURL = "\(GlobalURL.baseURL1)\(GlobalURL.orderAcceptRejectURL)"
//            print(apiAcceptRejectURL)
            
            let newTodo: [String: Any] = ["order_id": "\(orderId)", "order_status": "\(acceptRejectStatus)"]
            
            let header = ["Authorization": "Bearer "+token]
            
//            print(header)
//            UtilityMethods.showIndicator(withMessage: "Please wait...")
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(apiAcceptRejectURL, method: .post, parameters: newTodo, encoding: URLEncoding.default, headers: header).responseJSON { response in
                
                MBProgressHUD.hideHUD()
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        
                        
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
                            action in
                            
                            self.dismiss(animated: true, completion: nil)
                            
                            if self.isfromNotification == "yes" {
                                
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                                
                                 vc.modalPresentationStyle = .fullScreen
                                 self.present(vc, animated: true, completion: nil)
                                
                            }
                            else{
                                NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)
                            }
                            
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    } else {
                        UtilityMethods.hideIndicator()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
                            action in
                            
                           self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
    
    
    func reject(){
        
        if Reachability.isConnectedToNetwork() {
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            
            let apiRejectURL = "\(GlobalURL.baseURL1)\(GlobalURL.orderRejectURL)"
           
            
            let newTodo: [String: Any] = [ "id": "\(popupDict["id"]!)"]
            
            let header = ["Authorization": "Bearer "+token]
          
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(apiRejectURL, method: .post, parameters: newTodo, encoding: URLEncoding.default, headers: header).responseJSON { response in
                
                MBProgressHUD.hideHUD()
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
                    //                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        
//                        NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
                            action in
                            
                            
                            
                            if self.isfromNotification == "yes" {
                                
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                                
                                 vc.modalPresentationStyle = .fullScreen
                                 self.present(vc, animated: true, completion: nil)
                                
                            }
                            self.dismiss(animated: true, completion: nil)
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    } else {
                        UtilityMethods.hideIndicator()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
                            action in
                            
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    

}
