//
//  SignInVc.swift
//  Shopfeur Users
//
//  Created by deepkohli on 01/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var fromLogin = false

class SignInVc: UIViewController, UITextFieldDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var presenter: SignInViewToPresenterProtocol?
    var window: UIWindow?
    
    @IBOutlet weak var txt_mobileNumber: UITextField!
    @IBOutlet weak var txt_Password: CustomTextField!
    @IBOutlet weak var txt_PasswordRound: UITextField!
    @IBOutlet weak var butt_signin: UIButton!
    @IBOutlet weak var lbl_signup: UILabel!
    
    required init(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_Password.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
        txt_mobileNumber.layer.cornerRadius = txt_mobileNumber.frame.height/2
        txt_mobileNumber.setLeftPaddingPoints(20)
        txt_mobileNumber.setRightPaddingPoints(10)
        txt_PasswordRound.layer.cornerRadius = txt_PasswordRound.frame.height/2
        txt_Password.layer.cornerRadius = txt_Password.frame.height/2
        txt_Password.delegate = self
        txt_Password.setLeftPaddingPoints(20)
        txt_Password.setRightPaddingPoints(10)
        butt_signin.layer.cornerRadius = butt_signin.frame.height/2
        self.lbl_signup.attributedText = concatAttributedString(firstString: AppConstants.Donthaveaccount, secondString: AppConstants.signup)
    }
    
    @IBAction func action_Signin(_ sender: Any) {
        
        
        self.view.endEditing(true)
        if txt_mobileNumber.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.EmptyMobileNumber)
        }

        else if txt_Password.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.EmptyPassword)
        } else {
            UserDefaults.standard.set(txt_mobileNumber.text!, forKey: "MOBILENUMBER")

            apiSignIn()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txt_Password.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txt_Password.isSecureTextEntry = true
        
    }
    
    @IBAction func action_Password_ShowHide(_ sender: Any) {
        if PasswordShowHide(textfield: txt_Password) {
            txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeHide")
        } else {
           txt_Password.imgViewRightIcon?.image = #imageLiteral(resourceName: "EyeShow")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_mobileNumber {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    @IBAction func action_gotoSignup(_ sender: Any) {
        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
        if let view = storyboard.instantiateViewController(withIdentifier: "SignupVCs") as? SignupVCs {
            self.navigationController?.pushViewController(view, animated: true)
        }
        //appDelegate.callSignupController()
    }
    
    @IBAction func gotoForgotPassword(_ sender: Any) {
        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
        if let view = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVCs") as? ForgotPasswordVCs {
            self.navigationController?.pushViewController(view, animated: true)
        }
        //appDelegate.callForgotController()
    }
    
    func apiSignIn() {
        
        let Token = UserDefaults.standard.value(forKey: "fcm_token") as? String ?? "asdfg"
        
        let apiSignInURL = "\(GlobalURL.baseURL)\(GlobalURL.loginURL)"
//        print(apiSignInURL)
        
        
        let newTodo: [String: Any] = ["user_name": "\(txt_mobileNumber.text ?? "")", "password": "\(txt_Password.text ?? "")", "device_type": "2", "device_token": Token]
        
//        print("parameters are \(newTodo)")
        
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(apiSignInURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            
            MBProgressHUD.hideHUD()
            if (response.result.value != nil){
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()

                    let responceJson = swiftyJsonVar["result"]
//                    print(responceJson)
                    UserDefaults.standard.set(responceJson["token"].stringValue, forKey: "authToken")
                    
                    let otp = responceJson["otp"].stringValue
                    UserDefaults.standard.set(otp, forKey: "OTP")
                    
                    if responceJson["otp_verify"].intValue == 1{
                        
                        UserDefaults.standard.set(responceJson["id"].intValue, forKey: "user_Id")
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                        
                         vc.modalPresentationStyle = .fullScreen
                         self.present(vc, animated: true, completion: nil)
                        
                    }else{
                    
                    UserDefaults.standard.set(self.txt_mobileNumber.text, forKey: "MOBILE")
                    UserDefaults.standard.set(responceJson["rating"]["ratings_average"].doubleValue, forKey: "rating")
//                    UserDefaults.standard.set(responceJson["id"].intValue, forKey: "user_Id")
                    fromLogin = true
//                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "OtpScreenVCs") as! OtpScreenVCs
                    vc.comeFrom = "signin"
                    vc.custId = responceJson["id"].intValue
                    self.navigationController?.pushViewController(vc, animated: true)
                   // vc.modalPresentationStyle = .fullScreen
//                    self.present(vc, animated: true, completion: nil)
                    }
                    
                } else {
                    UtilityMethods.hideIndicator()
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}

extension SignInVc: SignInPresenterToViewProtocol {
    
    func fetchdata(dict: SignInEntities) {
        if dict.error! {
            statusBarErrorAlert(msg: dict.message!)
        } else {
            let otp_verify = dict.result?.object(forKey: "otp_verify") as? Int ?? 0
            if otp_verify == 1 {
                let first_name = dict.result?.object(forKey: "first_name") as? String ?? "Not Available"
                UserDefaults.standard.set(first_name, forKey: "NAME")
                UserDefaults.standard.set(dict.result?.object(forKey: "image"), forKey: "PROFILEDATA")
                UserDefaults.standard.synchronize()
                appDelegate.callRootViewController()
                UserDefaults.standard.set(true, forKey: AppConstants.LoginComplete)
                UserDefaults.standard.synchronize()
                statusBarSuccessAlert(msg: dict.message!)
            } else {
                let otp = dict.result?.object(forKey: "otp") as? Int ?? 0
                appDelegate.callVerifyOTPController()
                UserDefaults.standard.set(String(otp), forKey: "OTPP")
                UserDefaults.standard.synchronize()
                statusBarErrorAlert(msg: dict.message!)
            }
        }
    }
    
    func noInternet() {
        statusBarErrorAlert(msg: AppConstants.NoInternet)
    }
    
    func showAlert(title: String, msg: String) {
        showAlertDialog(title: title, description: msg)
    }
}
