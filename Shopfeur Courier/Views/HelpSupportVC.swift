//
//  HelpSupportVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HelpSupportVC: UIViewController, UITextViewDelegate {
    var labeltitle = ""
    @IBOutlet weak var txt_msgtitle: UITextField!
    @IBOutlet weak var txt_description: UITextView!
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var lineView: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        txt_msgtitle.layer.cornerRadius = 5.0
        txt_msgtitle.clipsToBounds = true
        txt_msgtitle.layer.borderWidth = 1
        txt_msgtitle.layer.borderColor = UIColor.black.cgColor
        txt_msgtitle.textColor = .black
        txt_msgtitle.setLeftPaddingPoints(10)
        
//        lbl_title.text = labeltitle
        
        lineView.textDropShadow()
        
        txt_description.layer.cornerRadius = 5.0
        txt_description.clipsToBounds = true
        txt_description.layer.borderWidth = 1
        txt_description.layer.borderColor = UIColor.black.cgColor
        txt_description.text = "Description"
        txt_description.backgroundColor = .clear
        txt_description.textColor = .lightGray
        txt_description.delegate = self
        
        cancelBtn.layer.masksToBounds = true
        cancelBtn.layer.cornerRadius = cancelBtn.frame.height/2
        submitBtn.layer.masksToBounds = true
        submitBtn.layer.cornerRadius = submitBtn.frame.height/2
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_description.text == "Description" {
            txt_description.text = ""
            txt_description.textColor = UIColor.black
        }
    }
    
    
    
    @IBAction func cancelAction(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func action_Submit(_ sender: Any) {
        
        
        
        if txt_msgtitle.text!.isEmpty {
            statusBarErrorAlert(msg: AppConstants.title)
        } else if txt_description.text!.isEmpty || txt_description!.text == "Description..." {
                   statusBarErrorAlert(msg: AppConstants.description)
        }else{
            
            self.helpMethod(title: txt_msgtitle.text!, message: txt_description.text!)
            
        }
        
    }
    
    
    func helpMethod(title:String,message:String){
            
                let apiChangeStatus = "\(GlobalURL.baseURL1)\(GlobalURL.helpAndSupport)"
//                print(apiChangeStatus)
                
                
                
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            let header = ["Authorization": "Bearer "+token]
            
            let param:[String:Any]=["title":title,"message":message]
            
//              print(param)
               
                UtilityMethods.showIndicator(withMessage: "Please wait...")
            
                Alamofire.request(apiChangeStatus, method: .post, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON { response in
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
//                            self.dismiss(animated: true, completion: nil)
                            let appDelegate = UIApplication.shared.delegate as? AppDelegate
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (UIAlertAction) in
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                                
                                 vc.modalPresentationStyle = .fullScreen
                                 self.present(vc, animated: true, completion: nil)
                            }))
                            
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    }
                   else {
                       UtilityMethods.hideIndicator()
                       
                       
                   }
                }
           
        }
    
    
    
    @IBAction func back_button(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
