//
//  ForgotPasswordVCs.swift
//  Shopfeur Users
//
//  Created by deepkohli on 01/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPasswordVCs: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var presenter: ForgotPasswordViewToPresenterProtocol?
    var window: UIWindow?

    @IBOutlet weak var txt_mobileNumber: UITextField!
    @IBOutlet weak var butt_send: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txt_mobileNumber.layer.cornerRadius = txt_mobileNumber.frame.height/2
        txt_mobileNumber.setLeftPaddingPoints(10)
        txt_mobileNumber.setRightPaddingPoints(10)
        butt_send.layer.cornerRadius = butt_send.frame.height/2
    }
    
    @IBAction func action_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_sendcode(_ sender: Any) {
        self.view.endEditing(true)
        if txt_mobileNumber.text!.isEmpty {
            statusBarErrorAlert(msg: "Mobile Number Field is Empty.")
        } 
         else if !txt_mobileNumber.text!.isValidPhoneNumber() {
         statusBarErrorAlert(msg: AppConstants.InvalidMobileNumber)

         }
        else {
//            presenter?.callForgotPasswordAPI(mobileNumber: txt_mobileNumber.text!.removingWhitespacesAndNewlines)
            otpByNumber()
        }
    }
    
    func otpByNumber() {
        
      
        let apiOtpByNumberURL = "\(GlobalURL.baseURL)\(GlobalURL.forgotPasswordURL)"
////        print(apiOtpByNumberURL)
        
        let newTodo: [String: Any] = ["mobile": "\(txt_mobileNumber.text ?? "")"]
//        print(newTodo)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        Alamofire.request(apiOtpByNumberURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
            MBProgressHUD.hideHUD()
            
            if (response.result.value != nil){
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()

                    //                    let responseJson = swiftyJsonVar["results"]
                    //we're OK to parse!
                    let otp = swiftyJsonVar["result"].stringValue
//                    print(otp)
                     UserDefaults.standard.set(otp, forKey: "OTPP")
                    UserDefaults.standard.set(self.txt_mobileNumber.text, forKey: "numberforpassword")
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Otp have sent to your mobile."
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {action in
                        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
                        if let view = storyboard.instantiateViewController(withIdentifier: "OtpScreenVCs") as? OtpScreenVCs {
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                        }))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    UtilityMethods.hideIndicator()
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
}

extension ForgotPasswordVCs: ForgotPasswordPresenterToViewProtocol {
    
    func fetchforgotdata(dict: ForgotPasswordEntities) {
         if dict.error! {
            statusBarErrorAlert(msg: dict.message!)
         } else {
            let otp = dict.result?.object(forKey: "otp") as? String ?? ""
            appDelegate.callVerifyOTPController()
            UserDefaults.standard.set(otp, forKey: "OTPP")
            UserDefaults.standard.synchronize()
        }
    }
    
    func noInternet() {
        statusBarErrorAlert(msg: AppConstants.NoInternet)
    }
    
    func showAlert(title: String, msg: String) {
        showAlertDialog(title: title, description: msg)
    }
    
    func showStatusSuccess(msg: String) {
        appDelegate.callSigninController()
        statusBarSuccessAlert(msg: msg)
    }
}
