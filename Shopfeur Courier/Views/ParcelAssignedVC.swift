//
//  ParcelAssignedVC.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 23/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class ParcelAssignedVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var orderId = Int()
    var productId = Int()
    var orderStatus = Int()
    var orderType = ""
    var sourceContact = String()
    var finalContact = String()
    //var orderDetails = JSON()
    var orderDetails : NSDictionary?
    var refreshControl = UIRefreshControl()
    var indexNumber = NSIndexPath()
    
    var productImageStatus = Int()
    
    //var orderList = JSON()
    
    var orderList: [NSDictionary] = []
    
    var imagePicker = UIImagePickerController()
    var image = UIImage()
    var imgData = Data()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
//        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getOrderDetailsMethod()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        imagePicker.delegate = self
        
        
        
    }
    
    @objc func refresh() {
//        MBProgressHUD.showHUDMessage(message: "Getting order for you...", PPview: self.view)
        self.getOrderDetailsMethod()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.orderList.count > 0 {
            return 2
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard self.orderDetails != nil else { return 0 }
        if section == 0 {
            return 3
        }else{
            return self.orderList.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        if indexPath.section == 0{
            guard let orderDetails = self.orderDetails else {return cell}
            if indexPath.row == 0{
                
                let sourcecell = tableView.dequeueReusableCell(withIdentifier: "ParcelCell") as! ParcelTableViewCell
                
                sourcecell.backView.layer.cornerRadius = 10
                sourcecell.backView.layer.shadowColor = UIColor.gray.cgColor
                sourcecell.backView.layer.shadowOpacity = 1
                sourcecell.backView.layer.shadowOffset = CGSize.zero
                sourcecell.backView.layer.shadowRadius = 5
                
                
                self.roundCorners(radius: 10, view: sourcecell.sideView)
                
                sourcecell.btnCall.layer.masksToBounds = true
                sourcecell.btnCall.layer.cornerRadius = sourcecell.btnCall.frame.height/2
                if orderType == "order"{
                    
                    guard let orderDetails = orderDetails["store"] as? NSDictionary else {return cell}
                    
                    sourcecell.lblPerson.text = orderDetails["name"] as? String ?? ""
                    sourcecell.lblAddress.text = orderDetails["address"] as? String ?? ""
                    self.finalContact = orderDetails["mobile"] as? String ?? ""
                    
                }else{
                    
                    sourcecell.lblPerson.text = orderDetails["from_name"] as? String ?? ""
                    sourcecell.lblAddress.text = orderDetails["from_pickup_address"] as? String ?? ""
                    self.finalContact = orderDetails["from_mobile"] as? String ?? ""
                    
                }
               
                cell = sourcecell
                
                
            }
            else if indexPath.row == 1{
                
                let finalcell = tableView.dequeueReusableCell(withIdentifier: "FinalCell") as! FinalCell
                
                
                finalcell.bgView.layer.cornerRadius = 10
                
                finalcell.bgView.layer.shadowColor = UIColor.gray.cgColor
                finalcell.bgView.layer.shadowOpacity = 1
                finalcell.bgView.layer.shadowOffset = CGSize.zero
                finalcell.bgView.layer.shadowRadius = 5
                
                
                
                self.roundCorners(radius: 10, view: finalcell.sideView)
                
                finalcell.FinalBtnCall.layer.masksToBounds = true
                finalcell.FinalBtnCall.layer.cornerRadius = finalcell.FinalBtnCall.frame.height/2
                
                if orderType == "order"{
                   
                    
                    let fullName = "\(orderDetails["billing_address"] as? String ?? "") \(orderDetails["billing_city"] as? String ?? "") \(orderDetails["billing_state"] as? String ?? "") \(orderDetails["billing_zipcode"] as? String ?? "")"
                                   finalcell.lblfinalName.text = orderDetails["billing_name"] as? String ?? ""
                                   finalcell.lblFinalAddress.text = "\(fullName)"
                                   self.sourceContact = orderDetails["billing_number"] as? String ?? ""
                                   
                               }else{
                    
                    finalcell.lblfinalName.text = orderDetails["to_name"] as? String ?? ""
                    finalcell.lblFinalAddress.text = orderDetails["to_pickup_address"] as? String ?? ""
                                   
//                                   finalcell.lblfinalName.text = orderDetails["from_name"] as? String ?? ""
//                                   finalcell.lblFinalAddress.text = "\(fullName)"
                                   self.sourceContact = orderDetails["to_mobile"] as? String ?? ""
                               }
                
                cell = finalcell
                
                
            }else {
                
                let bottomcell = tableView.dequeueReusableCell(withIdentifier: "BottomCell") as! BottomCell
                
                
                bottomcell.bgView.layer.cornerRadius = 10
                bottomcell.bgView.layer.shadowColor = UIColor.gray.cgColor
                bottomcell.bgView.layer.shadowOpacity = 1
                bottomcell.bgView.layer.shadowOffset = CGSize.zero
                bottomcell.bgView.layer.shadowRadius = 5
                
                bottomcell.addressImageView.layer.cornerRadius = 10.0
                bottomcell.addressImageView.layer.masksToBounds = true
                
                
                let gesture = UITapGestureRecognizer(target: self, action: #selector(mapViewAction(_:)))
                bottomcell.addressImageView.addGestureRecognizer(gesture)
                
                bottomcell.bgView.addGestureRecognizer(gesture)
                
                cell = bottomcell
                
                
                
            }
        } else {
            
//            print(orderList)
            
            let productcell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsCell") as! OrderDetailsCell
            
            if let orderList = orderList[indexPath.row] as? NSDictionary {
                if let productDetails = orderList["product"] as? NSDictionary{
                    
                    productcell.productName.text = productDetails["name"] as? String ?? ""
                    
                    let htmlText = productDetails["short_description"] as? String ?? ""
                    productcell.productDetails.text = htmlText.htmlToString
//                    productcell.productDetails.text = productDetails["short_description"] as? String ?? ""
                    
                    let quantity = "\(String(describing: orderList["quantity"]!) )"
                    
                    productcell.productQuantity.text = String(format: "Qty : %@",quantity )
                    
                    productcell.productPrice.text = String(format:  "$%@", orderList["order_price"] as? String ?? "")
                    
                    
                    let imgUrl = "\(GlobalURL.imagebaseURL)\(productDetails["featured_image"] as? String ?? "")"
                    
                    productcell.productImageView.sd_setImage(with: URL(string: "\(imgUrl)"), placeholderImage: #imageLiteral(resourceName: "NoImage"))
                    
                    /*
                     //clicked product image status image_status
                       //0=Pending,1=Uploaded,2=Accepted,3=Rejected
                       const val STATUS_CLICK_PENDING = 0
                       const val STATUS_CLICK_UPLOADED = 1
                       const val STATUS_CLICK_ACCEPTED = 2
                       const val STATUS_CLICK_REJECTED = 3
                     */
                    
                    let imageStatus = orderList["image_status"] as? Int ?? 0
                    if imageStatus == 0 {
                        
                        productcell.uploadImgBtn.setTitle("UPLOAD IMAGE", for: .normal)
                    }else if imageStatus == 1 {
                        
                        productcell.uploadImgBtn.setTitle("UPLOADED", for: .normal)
//                        productcell.uploadImgBtn.isEnabled = false
                    }else if imageStatus == 2 {
                        
                        productcell.uploadImgBtn.setTitle("ACCEPTED", for: .normal)
//                        productcell.uploadImgBtn.isEnabled = false
                    }else {
                        
                        productcell.uploadImgBtn.setTitle("REJECTED", for: .normal)
                        self.productImageStatus = imageStatus
                        //productcell.uploadImgBtn.isEnabled = false
                    }
                    
                    
                    
                }
            }
            
            
            
            cell = productcell
            
        }
        
        return cell
        
    }
    
    
    @IBAction func uploadImageAction(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at:buttonPosition)
        
        indexNumber = indexPath! as NSIndexPath
        
        if let orderList = orderList[indexPath?.row ?? 0] as? NSDictionary {
        if let productDetails = orderList["product"] as? NSDictionary{
            
            self.productId = productDetails["id"] as? Int ?? 0
//            print(self.productId)
            }
        }
        
        
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        uploadOrderImageApi()
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func roundCorners(radius: CGFloat , view:UIView) {
        view.layer.cornerRadius = radius
        view.clipsToBounds = true
        view.layer.maskedCorners = [.layerMinXMinYCorner,  .layerMinXMaxYCorner]
    }
    
    @objc func outForDeliveryAction(_ sender: Any) {
         
        
        
        outForDeliveryMethod()
        //print("out for delivery")
    }
    
    @IBAction func sourceCallAction(_ sender: Any) {
        
        if sourceContact.isEmpty{
            
            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Phone number not available"
                , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            let tempCall = String(format:"%@",sourceContact)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
    }
    
    @IBAction func finalCallAction(_ sender: Any) {
        if finalContact.isEmpty{
            
            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Phone number not available"
                , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            let tempCall = String(format:"%@",finalContact)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
        
    }
    
    
    @objc func mapViewAction(_ sender:UITapGestureRecognizer){
        
        guard let orderDetails = self.orderDetails else {return }
        
        let store_lat = Double("\(orderDetails["store_lat"]!)")
        let store_long = Double("\(orderDetails["store_lng"]!)")
        
        let order_lat = Double("\(orderDetails["order_lat"]!)") //as? Double ?? 0.0
        let order_long = Double("\(orderDetails["order_lng"]!)") //as? Double ?? 0.0
        

        
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=\(store_lat!),\(store_long!)&daddr=\(order_lat!),\(order_long!)&zoom=20&views=traffic")!)
        }
        else {
//            print("Can't use comgooglemaps://");
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 80))
        
        if orderType == "order"{
            if section == 1{
                if orderStatus == 3{
                let button = UIButton(frame: CGRect(x: 40, y: 10, width: view.bounds.width-80 , height: 40))
                button.setTitle("Item delivered >", for: .normal)
                button.setTitleColor(.blue, for: .normal)
                    button.layer.cornerRadius = 5
                    button.layer.borderColor = UIColor.blue.cgColor
                    button.layer.borderWidth = 2
                    
                button.addTarget(self, action: #selector(itemDeliveredAction(_:)), for: .touchUpInside)
                button.backgroundColor = .white
                
                footerView.addSubview(button)
                }else{
                    
                    let button = UIButton(frame: CGRect(x: 40, y: 10, width: view.bounds.width-80 , height: 40))
                    button.setTitle("Out for delivery", for: .normal)
                    button.setTitleColor(.white, for: .normal)
                    button.addTarget(self, action: #selector(outForDeliveryAction(_:)), for: .touchUpInside)
                    button.backgroundColor = .blue
                    
                    footerView.addSubview(button)
                    
                }
            }
        }else{
            
            if section == 0{
               if orderStatus == 3{
                let button = UIButton(frame: CGRect(x: 40, y: 10, width: view.bounds.width-80 , height: 40))
                button.setTitle("Item delivered >", for: .normal)
                button.setTitleColor(.init(red: 93/255.0, green: 122/255.0, blue: 248/255.0, alpha: 1.0), for: .normal)
                    button.layer.cornerRadius = 5
                button.layer.borderColor = UIColor.init(red: 93/255.0, green: 122/255.0, blue: 248/255.0, alpha: 1.0).cgColor
                    button.layer.borderWidth = 2
                button.titleLabel?.font = UIFont(name: "poppins_regular", size: 20.0)
                    
                button.addTarget(self, action: #selector(itemDeliveredAction(_:)), for: .touchUpInside)
                button.backgroundColor = .white
                
                footerView.addSubview(button)
                }else{
                    
                    let button = UIButton(frame: CGRect(x: 40, y: 10, width: view.bounds.width-80 , height: 40))
                    button.setTitle("Out for delivery", for: .normal)
                    button.setTitleColor(.white, for: .normal)
                    button.addTarget(self, action: #selector(outForDeliveryAction(_:)), for: .touchUpInside)
                button.titleLabel?.font = UIFont(name: "poppins_regular", size: 20.0)
                button.backgroundColor = .init(red: 93/255.0, green: 122/255.0, blue: 248/255.0, alpha: 1.0)
                    
                    footerView.addSubview(button)
                    
                }
                
            }
            
        }
        return footerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 80
        }else{
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: tableView.sectionHeaderHeight))
        
        guard let orderDetails = self.orderDetails else {return headerView}
        
        if section == 0{
            let lblAmount = UILabel(frame: CGRect(x: 20, y: 10, width: view.bounds.width-20, height: 30))
            lblAmount.text = "Amount Status"
            lblAmount.font = UIFont(name: "poppins_regular", size: 15.0)
            lblAmount.textColor = .darkGray
            
            let lblPrice = UILabel(frame: CGRect(x: 20, y: 40, width: view.bounds.width-20, height: 30))
//            lblPrice.text = String(format: "$%@", orderDetails["delivery_charge"] as? String ?? "")
            lblPrice.text = "Paid"
            lblPrice.font = UIFont(name: "poppins_regular", size: 25.0)
            lblPrice.textColor = .orange
            
            headerView.addSubview(lblAmount)
            headerView.addSubview(lblPrice)
            
        }else{
            
            let orderDetails = UILabel(frame: CGRect(x: 20, y: 0, width: view.bounds.width-20, height: 40))
            orderDetails.text = "Order Details"
            orderDetails.font = UIFont(name: "poppins_regular", size: 20.0)
            orderDetails.textColor = .darkGray
            headerView.addSubview(orderDetails)
        }
        
        return headerView
    }
    
    
    @objc func itemDeliveredAction(_ sender:UIButton){
        
//        if productImageStatus > 2{
//            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Product Image has been rejected by the user. Please upload again..."
//                , preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//
//        }else{
//
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SignatureVC") as! SignatureVC
        VC.orderID = self.orderId
        
        self.navigationController?.pushViewController(VC, animated: true)
        
       
//        }
        
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    func getOrderDetailsMethod(){
        
        let apiGetDetails = "\(GlobalURL.baseURL1)\(GlobalURL.userOrderDetailsURL)?order_id=\(orderId)"
        
//        print(apiGetDetails)
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let header = ["Authorization": "Bearer "+token]
        
        let param = ["order_id":"\(orderId)"]
//        print(param)
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        
        Alamofire.request(apiGetDetails, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: header).responseJSON { response in
            
            MBProgressHUD.hideHUD()
            MBProgressHUD.hide(for: self.view, animated: true)
            UtilityMethods.hideIndicator()
            if (response.result.value != nil){
                self.refreshControl.endRefreshing()
                if let resultDict = response.result.value as? NSDictionary {
                    if let result = resultDict["result"] as? NSDictionary {
                        self.orderDetails = result
                        if let orders = result["order_items"] as? [NSDictionary], orders.count > 0 {
                            self.orderList = orders
                        }
                        DispatchQueue.main.async {
                            
                            self.tableView.reloadData()
                        }
                    }else{
                        UtilityMethods.hideIndicator()
                        
                        self.tableView.isHidden = true
                        let swiftyJsonVar = JSON(response.result.value!)
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                
                
               
            }
            
        }
        
        
    }
    
    
    func uploadOrderImageApi() {
        let apiUploadOrderImageURL = "\(GlobalURL.baseURL1)\(GlobalURL.uploadOrderImageURL)"
        
        let params = ["order_id" : orderId, "product_id": productId]
       
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        
        imgData = image.jpegData(compressionQuality: 0.4)!
        
        let url = apiUploadOrderImageURL
//        print(url)
//        print(params)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+token ,
            "Content-type": "multipart/form-data"
        ]
//        print(headers)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(self.imgData, withName: "order_image", fileName: "image.png", mimeType: "image/png")
            
        }, to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    MBProgressHUD.hideHUD()
                    
//                    print("Succesfully uploaded")
                            
                    if (response.result.value != nil){
                        
                        let swiftyVarJson = JSON(response.result.value!)
                        
//                        print(swiftyVarJson)
                        
                        if swiftyVarJson["error"] == false{
                        
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message:
                                swiftyVarJson["message"].stringValue
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                                self.getOrderDetailsMethod()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyVarJson["message"].stringValue
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                                self.getOrderDetailsMethod()
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                            

                    }
                }
                    
            case .failure(let error):
                MBProgressHUD.hideHUD()
//                print("Error in upload: \(error.localizedDescription)")
             let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: "Image uploading failed"
                                            , preferredStyle: .alert)
                                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    
    func outForDeliveryMethod(){
        
                let apiChangeStatus = "\(GlobalURL.baseURL1)\(GlobalURL.outForDelivery)"
//                print(apiChangeStatus)
                
                
                
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            let header = ["Authorization": "Bearer "+token]
            
            let param:[String:Any]=["order_id":"\(orderId)"]
            
//              print(param)
               
//                UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
                Alamofire.request(apiChangeStatus, method: .post, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
                            
                            NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)
                            
                            self.orderStatus = 3
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                            
                            
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    }
                   else {
                       UtilityMethods.hideIndicator()
                       
                       
                   }
                }
       
    }
    
    
    
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


