//
//  AssignedOrderVC.swift
//  Shopfeur Courier
//
//  Created by Sushant on 29/11/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class AssignedOrderVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    let orderid = UserDefaults.standard.value(forKey: "orderID") as? Int ?? 0
    var arrOrderDetails = JSON()
    var arrOrderItems = JSON()
    var productId = Int()
    
    var imagePicker = UIImagePickerController()
    var image = UIImage()
    var imgData = Data()
    
    
    @IBOutlet weak var tableViewAssignedOrder: UITableView!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        orderDetailsAPI()
        
    }
    
    
    @IBAction func back_Button_Action(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else {
            return self.arrOrderItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "AssignedOrderCell", for: indexPath) as! AssignedOrderCell
        
            
            
            return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else if indexPath.section == 1 {
            return 226
            
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 250
        } else if indexPath.section == 2{
            return 500
        } else {
            return 0
        }
    }
    
    @objc func addImage(_ sender: UIButton){
        // use the tag of button as index
        
        
        self.productId = self.arrOrderItems[sender.tag]["product_id"].intValue
//        print(self.productId)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        uploadOrderImageApi()
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func orderDetailsAPI() {
        
        if Reachability.isConnectedToNetwork() {
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            
            let apiOrderDetailsURL = "\(GlobalURL.baseURL1)\(GlobalURL.orderDetailsURL)"
//            print(apiOrderDetailsURL)
            let header = ["Authorization": "Bearer "+token]
            
            let params = ["order_id" : orderid]
            
//            print(header)
            UtilityMethods.showIndicator(withMessage: "Please wait...")
            
            Alamofire.request(apiOrderDetailsURL, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { response in
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()
                        
                        self.arrOrderDetails = swiftyJsonVar["result"]
                        
                        self.arrOrderItems = self.arrOrderDetails["order_items"]
                        
//                        print(self.arrOrderDetails)
                        
                        self.tableViewAssignedOrder.reloadData()
                        
                        
                    } else {
                        UtilityMethods.hideIndicator()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
    func uploadOrderImageApi() {
        let apiUploadOrderImageURL = "\(GlobalURL.baseURL1)\(GlobalURL.uploadOrderImageURL)"
        
        let params = ["order_id" : "\(orderid)", "product_id": "\(productId)"]
       
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        
        imgData = image.jpegData(compressionQuality: 0.4)!
        
        let url = apiUploadOrderImageURL
//        print(url)
//        print(params)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+token ,
            "Content-type": "multipart/form-data"
        ]
//        print(headers)
        
        UtilityMethods.showIndicator(withMessage: "Please wait...")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(self.imgData, withName: "order_image", fileName: "image.png", mimeType: "image/png")
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
//                    print("Succesfully uploaded")
                    //                    if let err = response.error {
                    //
                    //                        return
                    //                    }
                    
//                    print("Done")
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
                            
                            let responceJson = swiftyJsonVar["result"]
//                            print(responceJson)
                            
                    
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    

}
