//
//  UserProfileVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 23/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import SDWebImage

class UserProfileVC: UIViewController {
    
    @IBOutlet weak var img_Profilepic: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_PhoneNumber: UITextField!
    @IBOutlet weak var txt_Address: UITextField!
    @IBOutlet weak var btn_Submit: UIButton!
    
    @IBOutlet weak var lineView: UILabel!
    
    var name = String()
    var email = String()
    var phone = String()
    var dob = String()
    var gender = String()
    var imageURL = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if let data = UserDefaults.standard.object(forKey: "dictionary") as? NSData {
//            let dict = NSKeyedUnarchiver.unarchiveObject(with: data as Data)
//            print(dict as Any)
//        }
        
        lineView.textDropShadow()
        
        txt_email.isUserInteractionEnabled = false
        txt_PhoneNumber.isUserInteractionEnabled = false
        txt_Address.isUserInteractionEnabled = false
        

        
        img_Profilepic.layer.cornerRadius = img_Profilepic.frame.height/2
        img_Profilepic.layer.borderColor = UIColor.lightGray.cgColor
        img_Profilepic.layer.shadowColor = UIColor.darkGray.cgColor
        img_Profilepic.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        img_Profilepic.layer.shadowRadius = 25.0
        img_Profilepic.layer.shadowOpacity = 0.9
        img_Profilepic.layer.borderWidth = 1.0
        img_Profilepic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        img_Profilepic.sd_imageTransition = .fade
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        let imgURL = imageURL
//        print(imgURL)
        self.img_Profilepic.sd_setImage(with: URL(string: imgURL), placeholderImage: #imageLiteral(resourceName: "NoProfilePic"))
        
        lbl_name.text = name
        txt_email.text = gender
        txt_Address.text = dob
        txt_PhoneNumber.text = phone
    }
    
    
    @IBAction func bellAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func back_Button(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func editButtonAction(_ sender: Any) {
        txt_email.isUserInteractionEnabled = true
        txt_PhoneNumber.isUserInteractionEnabled = true
        txt_Address.isUserInteractionEnabled = true
        btn_Submit.isHidden = false
        txt_email.becomeFirstResponder()
        
        
    }
    
    
}
