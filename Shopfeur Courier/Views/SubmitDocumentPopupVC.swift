//
//  SubmitDocumentPopupVC.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 13/02/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class SubmitDocumentPopupVC: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

       bgView.layer.cornerRadius = 10.0
            bgView.layer.masksToBounds = true
            
            submitBtn.layer.cornerRadius = submitBtn.frame.height/2
            submitBtn.layer.masksToBounds = true
            
            self.applyGradient(colours: [.blue,.init(red: CGFloat(6/255.0), green: CGFloat(0.0/255.0), blue: CGFloat(104.0/255.0), alpha: 1.0)], view: submitBtn)
            
            
        }
        
        func applyGradient(colours: [UIColor],view:UIView)  {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = view.bounds
            gradient.colors = colours.map { $0.cgColor }
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 0)
            view.layer.insertSublayer(gradient, at: 0)
            
        }
    

   @IBAction func notNowAction(_ sender: Any) {
    
    
            
            self.dismiss(animated: true, completion: nil)
        }
        
        @IBAction func submitAction(_ sender: Any) {
            
            let upload = self.storyboard?.instantiateViewController(withIdentifier: "DocumentUploadVC") as! DocumentUploadVC
            upload.modalPresentationStyle = .fullScreen
            upload.isfromMenu = "YES"
            self.present(upload, animated: true, completion: nil)
            
//            print("Document verification")
        }

}
