//
//  LeftmenuVC.swift
//  Shopfeur Users
//
//  Created by deepkohli on 04/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON


class LeftmenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var img_profilepic: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var tableview_sidebar: UITableView!
    
    @IBOutlet weak var documentBtn: UIButton!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingValueLabel: UILabel!
    
    var driverName = String()
    var driverEmail = String()
    var dateofbirth = String()
    var gender = String()
    let driverAddress = String()
    var driverPhone = String()
    var driverImageURL = String()
    var status = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        
        
        
        
        img_profilepic.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(action_profile(_:)))
        img_profilepic.addGestureRecognizer(gesture)
        
        img_profilepic.layer.cornerRadius = img_profilepic.frame.height/2
        img_profilepic.layer.borderColor = UIColor.lightGray.cgColor
        img_profilepic.layer.shadowColor = UIColor.darkGray.cgColor
        img_profilepic.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        img_profilepic.layer.shadowRadius = 25.0
        img_profilepic.layer.shadowOpacity = 0.9
        img_profilepic.layer.borderWidth = 1.0
        img_profilepic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        img_profilepic.sd_imageTransition = .fade
        lbl_name.text = UserDefaults.standard.value(forKey: "NAME") as? String ?? "Not Available"
        lbl_email.text = UserDefaults.standard.value(forKey: "MOBILENUMBER") as? String ?? "Not Available"
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tableview_sidebar.tableFooterView = UIView()
        tableview_sidebar.delegate = self
        tableview_sidebar.dataSource = self
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableview_sidebar.tableHeaderView = UIView(frame: frame)
        tableview_sidebar.tableFooterView = UIView(frame: frame)
        
        getDriverProfileAPI()
    }
    
    override var prefersStatusBarHidden: Bool {
           return true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
           return .fade
    }
    
    
    
    @IBAction func documentVerificationAction(_ sender: Any) {
        
        
        let upload = self.storyboard?.instantiateViewController(withIdentifier: "DocumentUploadVC") as! DocumentUploadVC
        upload.modalPresentationStyle = .fullScreen
        upload.isfromMenu = "YES"
        self.present(upload, animated: true, completion: nil)
        
//        print("Document verification")
        
        
    }
    
    
    @IBAction func action_profile(_ sender: Any) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        profile.name = driverName
        profile.email = driverEmail
        profile.phone = driverPhone
        profile.imageURL = driverImageURL
        profile.gender = self.gender
        profile.dob = self.dateofbirth
        profile.modalPresentationStyle = .fullScreen
        self.present(profile, animated: true, completion: nil)
    }
    
    @objc func action_Orders(_ sender: UITapGestureRecognizer) {
        self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderHistoryVC")), animated: true)
        
        self.sideMenuViewController!.hideMenuViewController()
    }
    
    @IBAction func action_Notifications(_ sender: Any) {
       let notify = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        notify.modalPresentationStyle = .fullScreen
        self.present(notify, animated: true, completion: nil)
    }
    
    @IBAction func action_HelpSupport(_ sender: Any) {
        let support = self.storyboard?.instantiateViewController(withIdentifier: "HelpSupportVC") as! HelpSupportVC
        support.modalPresentationStyle = .fullScreen
        self.present(support, animated: true, completion: nil)
    }
    
    @IBAction func terms_Conditions(_ sender: Any) {
        
        let privacypolicy = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        privacypolicy.labeltitle = "Terms & Condition"
        privacypolicy.modalPresentationStyle = .fullScreen
        self.present(privacypolicy, animated: true, completion: nil)
        
    }
    @IBAction func action_PrivacyPolicy(_ sender: Any) {
        let privacypolicy = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        privacypolicy.labeltitle = "Privacy Policy"
        privacypolicy.modalPresentationStyle = .fullScreen
        self.present(privacypolicy, animated: true, completion: nil)
    }
    
    @IBAction func action_Aboutus(_ sender: Any) {
        let aboutus = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        aboutus.labeltitle = "About us"
        aboutus.modalPresentationStyle = .fullScreen
        self.present(aboutus, animated: true, completion: nil)
    }
    
    @IBAction func action_LogOut(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVC") as! LogoutVC
                  self.present(vc, animated: true, completion: nil)
              } else {
                  // Fallback on earlier versions
                  let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVC") as! LogoutVC
                  self.present(vc, animated: true, completion: nil)
              }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: Leftmenucell = tableview_sidebar.dequeueReusableCell(withIdentifier: "firstcell", for: indexPath) as! Leftmenucell
            
            return cell
        } else if indexPath.row == 1 {
            let cell: Leftmenucell = tableview_sidebar.dequeueReusableCell(withIdentifier: "secondcell", for: indexPath) as! Leftmenucell
            
            return cell
        } else {
            let cell: Leftmenucell = tableview_sidebar.dequeueReusableCell(withIdentifier: "thirdcell", for: indexPath) as! Leftmenucell
            
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderHistoryVC")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        
        case 1:
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "NotificationsVC")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row==1{
            return 190
        }
        else{
        return 130
        }
    }
    
    
    
    func setCurrentUserDetails(user: NSDictionary) {
        do {
            try UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: user, requiringSecureCoding: true), forKey: "dictionary")
        }catch{
//            debugPrint(error.localizedDescription)
        }
    }
    
    
    func getDriverProfileAPI() {
        
        if Reachability.isConnectedToNetwork() {
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            let apiDriverProfiletURL = "\(GlobalURL.baseURL)\(GlobalURL.driverProfileURL)"
//            print(apiDriverProfiletURL)
            let header = ["Authorization": "Bearer "+token]
            
            UtilityMethods.showIndicator(withMessage: "Please wait...")
            
            Alamofire.request(apiDriverProfiletURL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
                UtilityMethods.hideIndicator()
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
//                        UtilityMethods.hideIndicator()
                        
                        let responseJson = swiftyJsonVar["result"]
//                        print(responseJson)
                        
                        
                        
/*if(data.work_permit_status == 0 && data.police_clearance_status == 0 && data.insurance_status == 0 && data.license_status == 0){
                        */
                        self.driverName  = responseJson["first_name"].stringValue
                        self.driverEmail = responseJson["email"].stringValue
                        if responseJson["gender"].stringValue == "1"{
                            self.gender  = "Male"
                        }else if responseJson["gender"].stringValue == "2"{
                            self.gender  = "Female"
                        }else if responseJson["gender"].stringValue == "3"{
                            self.gender  = "Other"
                        }else{
                            self.gender  = "Not available"
                        }
                        
                        if responseJson["dob"].stringValue == ""{
                            self.dateofbirth = "Not available"
                        }else{
                            self.dateofbirth = responseJson["dob"].stringValue
                        }
                        
                        
                        self.driverPhone = responseJson["country_code"].stringValue  + responseJson["mobile"].stringValue
                        
                        self.status = responseJson["ready_for_pickup"].intValue
//                        print("Name is \(self.driverName) and email Id is \(self.driverEmail)")
                        self.lbl_name.text = self.driverName
                        self.lbl_email.text = self.driverPhone
                        
                        self.ratingView.rating = responseJson["rating"]["ratings_average"].doubleValue
                        self.ratingValueLabel.text = String(format: "%.1f", responseJson["rating"]["ratings_average"].doubleValue )
                        
                        let bgURL = responseJson["image"].stringValue
                        self.driverImageURL = GlobalURL.imagebaseURL + bgURL
//                        print(self.driverImageURL)
                        
                        self.img_profilepic.sd_setImage(with: URL(string: "\(self.driverImageURL)"), placeholderImage: #imageLiteral(resourceName: "NoProfilePic"))
                        self.img_profilepic.clipsToBounds = true
                        
                        
                        
                        
                        
                        if responseJson["work_permit_status"].intValue == 0 && responseJson["police_clearance_status"].intValue == 0 && responseJson["insurance_status"].intValue == 0 && responseJson["license_status"].intValue == 0 {
                            
                            self.documentBtn.isHidden = false
                            self.documentBtn.setTitle("DOCUMENT NOT VERIFIED", for: .normal)
                            
                        }else if responseJson["driver_license"].stringValue == "" && responseJson["work_permit_file"].stringValue == "" || responseJson["police_clearance_file"].stringValue == "" || responseJson["insurance_file"].stringValue == ""{
                            
                            self.documentBtn.isHidden = false
                            self.documentBtn.setTitle("DOCUMENT NOT VERIFIED", for: .normal)
                            
                    }else{
                            self.documentBtn.isHidden = false
//                            self.documentBtn.isEnabled = false
                            self.documentBtn.setTitle("DOCUMENT VERIFIED", for: .normal)
                        }
                        
                        
                        

                        
                    } else {
//                        UtilityMethods.hideIndicator()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
    
    
}
