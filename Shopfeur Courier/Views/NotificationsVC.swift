//
//  NotificationsVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var labeltitle = ""
    @IBOutlet weak var tableview_notifications: UITableView!
    
    @IBOutlet weak var lineView: UILabel!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    
    var arrNotifications = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.notificationAPI()
        
        lineView.textDropShadow()

        // Do any additional setup after loading the view.
        tableview_notifications.tableFooterView = UIView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func back_button(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationsCell = tableview_notifications.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath) as! NotificationsCell
        
        cell.view_notifications.layer.cornerRadius = 10
        cell.view_notifications.layer.shadowColor = UIColor.gray.cgColor
        cell.view_notifications.layer.shadowOpacity = 1
        cell.view_notifications.layer.shadowOffset = CGSize.zero
        cell.view_notifications.layer.shadowRadius = 5
        
        cell.pendingBtn.layer.cornerRadius = 5.0
        cell.pendingBtn.layer.masksToBounds = true
        
        if let dataDict = arrNotifications[indexPath.row] as? [String:Any]{
            
//            cell.lblOrderid.text = "\(String(describing: dataDict["title"]!))"
            cell.lblOrderupadtesStatus.text = "\(String(describing: dataDict["title"]!))"
        }
        
//        cell.lbltime.text = arrNotifications[indexPath.row]["order_date"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @IBAction func pendingBtnAction(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableview_notifications)
        let indexPath = self.tableview_notifications.indexPathForRow(at:buttonPosition)!
        
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickupRequestVC") as! PickupRequestVC
        
        popvc.popupDict = self.arrNotifications[indexPath.row] as! [String:Any]
        popvc.modalPresentationStyle = .overCurrentContext
        popvc.isfromNotification = "yes"
        
        self.present(popvc, animated: true, completion: nil)
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func notificationAPI() {
        
        if Reachability.isConnectedToNetwork() {
            
            
            
            let apiNotificationURL = "\(GlobalURL.baseURL1)\(GlobalURL.allPendingNotifications)"
//            print(apiNotificationURL)
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            let header = ["Authorization": "Bearer "+token]
            
//            print(header)
//            UtilityMethods.showIndicator(withMessage: "Please wait...")
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(apiNotificationURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
                
                MBProgressHUD.hideHUD()
                if (response.result.value != nil){
                    if let swiftyJsonVar = response.result.value as? [String:Any]{
                        
                    if let error = swiftyJsonVar["error"] as? Bool{
                            if error == false{
                                
                                if let result = swiftyJsonVar["result"] as?[Any]{
                                    self.arrNotifications = result
                                }
                                
                                if self.arrNotifications.count > 0{
                                    self.tableview_notifications.reloadData()
                                }else{
                                    self.emptyView.isHidden = false
                                    self.tableview_notifications.isHidden = true
                                
                                }
                            }
                            else{
                                
                                
                            }
                        }
                    }
                    
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
    
}
