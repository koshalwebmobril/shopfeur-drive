//
//  OtherDocumentsVC.swift
//  Shopfeur Courier
//
//  Created by Sushant on 04/12/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MobileCoreServices

class OtherDocumentsVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var tf_Insurance: UITextField!
    @IBOutlet weak var tf_PoliceClearance: UITextField!
    @IBOutlet weak var tf_WorkPermit: UITextField!
    @IBOutlet weak var button_Done: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var permitText: UITextField!
   
    @IBOutlet weak var button_Insurance: UIButton!
    @IBOutlet weak var button_PoliceClearance: UIButton!
    @IBOutlet weak var button_WorkPermit: UIButton!
    
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var pickerBackView: UIView!
    @IBOutlet weak var dropdownBtn: UIButton!
   
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var imagePicker = UIImagePickerController()
    var image = UIImage()
    var imgData = Data()
    var parameters = [String:String]()
    var isfromMenu = String()
    
    @IBOutlet weak var backButtonWidth: NSLayoutConstraint!
    var pickerArray:[String] = ["Passport","Permanent Residence ID","Work Permit"]
    
    
    var flag = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        pickerView.removeFromSuperview()
        toolBar.removeFromSuperview()
        
        if isfromMenu.isEmpty{
            backButtonWidth.constant = 0.0
            backButton.isHidden = true
        }
        
//        tf_Insurance.rightViewMode = UITextField.ViewMode.always
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//        let image = UIImage(named: "ProductThumbnail")
//        imageView.image = image
//        tf_Insurance.rightView = imageView
        tf_Insurance.layer.cornerRadius = 5
        tf_PoliceClearance.layer.cornerRadius = 5
        tf_WorkPermit.layer.cornerRadius = 5
        permitText.layer.cornerRadius = 5
        
        permitText.layer.cornerRadius = 5
        permitText.inputAccessoryView = toolBar
        permitText.inputView = pickerView
        permitText.delegate = self
        
        permitText.rightView = dropdownBtn
        permitText.rightViewMode = .always

        
        tf_Insurance.clipsToBounds = true
        tf_PoliceClearance.clipsToBounds = true
        tf_WorkPermit.clipsToBounds = true
         permitText.clipsToBounds = true
        
        tf_Insurance.layer.borderWidth = 2
        tf_PoliceClearance.layer.borderWidth = 2
        tf_WorkPermit.layer.borderWidth = 2
        permitText.layer.borderWidth = 2
        
        tf_Insurance.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        tf_PoliceClearance.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        tf_WorkPermit.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        permitText.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        tf_Insurance.setLeftPaddingPoints(10)
        tf_PoliceClearance.setLeftPaddingPoints(10)
        tf_WorkPermit.setLeftPaddingPoints(10)
        permitText.setLeftPaddingPoints(10)
        
        button_Done.layer.cornerRadius = button_Done.frame.height/2
        button_Done.clipsToBounds = true
        
        self.progressBar.layer.cornerRadius = 5.0
        self.progressBar.layer.masksToBounds = true
        
        self.progressBar.clipsToBounds = true
        
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        toolBar.isHidden = false
//        pickerView.isHidden = f
        pickerBackView.isHidden = false
        return true
    }
    
    
    @IBAction func action_InsuranceBtn(_ sender: Any) {
        flag = 1
        chooseCameraGallery(sender)
        
    }
    @IBAction func action_ClearanceBtn(_ sender: Any) {
        flag = 2
        chooseCameraGallery(sender)
    }
    
    @IBAction func action_PermitBtn(_ sender: Any) {
        flag = 3
        chooseCameraGallery(sender)
    }
    @IBAction func action_DoneBtn(_ sender: Any) {
        if tf_Insurance.text == "" {
            statusBarErrorAlert(msg: AppConstants.UploadInsurance)
        } else if tf_PoliceClearance.text == "" {
            statusBarErrorAlert(msg: AppConstants.UploadPoliceClearance)
        } else if tf_WorkPermit.text == "" {
            statusBarErrorAlert(msg: AppConstants.UploadWorkPermit)
        } else  {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
            
            vc.modalPresentationStyle = .fullScreen
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    func chooseCameraGallery(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
//        alert.addAction(UIAlertAction(title: "PDF File", style: .default, handler: { _ in self.openDirectory()
            
//        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func openDirectory()
    {
        let types: [String] = [kUTTypePDF as String]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        self.present(documentPicker, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        if isfromMenu.isEmpty {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
//        print("import result : \(myURL)")
        let pdfURL = myURL
//        print(pdfURL)
        
        do{
            let myData = try Data(contentsOf: pdfURL)
//            print(myData)
            UploadDocumentPDF(pdfD: myData)
        }catch{
//            debugPrint(error.localizedDescription)
        }
        // you get from the urls parameter the urls from the files selected
    }
    
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
    

    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage 
//
//        self.imgString = "image"
        UploadDocument()
        dismiss(animated: true, completion: nil)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        permitText.text = pickerArray[row]
    }
    
    
    @IBAction func dropdownAction(_ sender: Any) {
        
        permitText.becomeFirstResponder()
        
//        pickerView.isHidden = false
//        toolBar.isHidden = false
    }
    
    @IBAction func cancelPickerAction(_ sender: Any) {
        
//        pickerView.resignFirstResponder()
        pickerBackView.isHidden = true
        permitText.resignFirstResponder()
    }
    
    @IBAction func donePickerAction(_ sender: Any) {
        
        
        let row = pickerView.selectedRow(inComponent: 0)
        permitText.text = pickerArray[row]
        
//        pickerView.resignFirstResponder()
        pickerBackView.isHidden = true
        permitText.resignFirstResponder()
    }
    
    func UploadDocument() {
        
        let apiUploadDocumentURL = "\(GlobalURL.baseURL1)\(GlobalURL.uploadOtherDocumentsURL)"
        
        if flag == 1 {
            parameters = ["document_type": "insurance"]
        } else if flag == 2 {
            parameters = ["document_type": "police_clearance"]
        } else if flag == 3 {
            if  permitText.text?.isEmpty ?? true{
               
                statusBarErrorAlert(msg: "Please select Work Permit")
                return
                
            }else{
                parameters = ["document_type": "work_permit","work_permit_type":"\(permitText.text!)"]
                
            
            }
        }
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        
        imgData = image.jpegData(compressionQuality: 0.4)!
        
        let url = apiUploadDocumentURL
//        print(url)
//        print(parameters)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+token ,
            "Content-type": "multipart/form-data"
        ]
//        print(headers)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in self.parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(self.imgData, withName: "driver_document", fileName: "image.png", mimeType: "image/png")
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    MBProgressHUD.hideHUD()
//                    print("Done")
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
                            
                            let responceJson = swiftyJsonVar["result"]
//                            print(responceJson)
                            
                            if self.flag == 1 {
                                self.tf_Insurance.text = "Image Uploaded"
                            } else if self.flag == 2 {
                                self.tf_PoliceClearance.text = "Image Uploaded"
                            } else if self.flag == 3 {
                                self.tf_WorkPermit.text = "Image Uploaded"
                            }
                            
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    
    func UploadDocumentPDF(pdfD: Data) {
        
        let apiUploadDocumentURL = "\(GlobalURL.baseURL1)\(GlobalURL.uploadOtherDocumentsURL)"
        
        if flag == 1 {
            parameters = ["document_type": "insurance"]
        } else if flag == 2 {
            parameters = ["document_type": "police_clearance"]
        } else if flag == 3 {
            parameters = ["document_type": "work_permit"]
        }
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        
        //let pdfD = Data()
        
//        imgData = image.jpegData(compressionQuality: 0.4)!
        
        let url = apiUploadDocumentURL
//        print(url)
//        print(parameters)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+token ,
            "Content-type": "multipart/form-data"
        ]
//        print(headers)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "Please wait...", PPview: self.view)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in self.parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(pdfD, withName: "driver_document", fileName: "document.pdf", mimeType: "application/pdf")
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    MBProgressHUD.hideHUD()
//                    print("Done")
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
                            
                            let responceJson = swiftyJsonVar["result"]
//                            print(responceJson)
                            
                            if self.flag == 1 {
                                self.tf_Insurance.text = "Pdf Uploaded"
                            } else if self.flag == 2 {
                                self.tf_PoliceClearance.text = "Pdf Uploaded"
                            } else if self.flag == 3 {
                                self.tf_WorkPermit.text = "Pdf Uploaded"
                            }
                            
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    

}
