//
//  VehiclePopupVC.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 14/02/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VehiclePopupVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var bgView: UIView!
    var arrVehicle = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        bgView.layer.masksToBounds = true
        bgView.layer.cornerRadius = 5
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup(_ :)))
    colorView.addGestureRecognizer(tapGesture)
        
        
        
    }
    
    @objc func dismissPopup(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVehicle.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleCell", for: indexPath) as! VehicleCell
        
        
        
        let imgURL = GlobalURL.imagebaseURL + arrVehicle[indexPath.row]["feature_image"].stringValue
        
        let escapedString = imgURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        cell.logoView.sd_setImage(with: URL(string:escapedString), placeholderImage: nil)
        
        cell.bikelabel.text = arrVehicle[indexPath.row]["name"].stringValue
        
        let fare  = arrVehicle[indexPath.row]["minimum_fare"].stringValue + " / " + arrVehicle[indexPath.row]["minimum_fare_meter"].stringValue
        
        cell.farelabel.text = "$\(fare) Meter"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60))
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: 60))
        label.text = "Vehicle Type"
        label.textAlignment = .center
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = arrVehicle[indexPath.row]["name"].stringValue
        let id = arrVehicle[indexPath.row]["id"].stringValue
        NotificationCenter.default.post(name: NSNotification.Name("setBike"), object: nil,userInfo:["name":name,"vehicle_type":id] )
        
        self.dismiss(animated: true, completion: nil)
        
    }
    

    

}
