//
//  WelcomeVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class WelcomeVC: UIViewController {
    
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var butt_getStarted: UIButton!
    @IBOutlet weak var yTPlayerView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        yTPlayerView.load(withVideoId: "VEDQ5rELtoU")
        
        butt_getStarted.layer.masksToBounds = true
        butt_getStarted.layer.cornerRadius = butt_getStarted.frame.height/2
    }
    
    
    @IBAction func getStartedAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:AppConstants.MainStoryboard,bundle: Bundle.main)
        if let view = storyboard.instantiateViewController(withIdentifier: "DocumentUploadVC") as? DocumentUploadVC {
            self.navigationController?.pushViewController(view, animated: true)
        }
        
    }
    
}
