//
//  SignatureVC.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 23/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import SwiftSignatureView
import Alamofire
import SwiftyJSON

class SignatureVC: UIViewController,SwiftSignatureViewDelegate {
    func swiftSignatureViewDidTapInside(_ view: SwiftSignatureView) {
        
    }
    
    func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView, _ pan: UIPanGestureRecognizer) {
        
    }
    

    @IBOutlet weak var signatureView: SwiftSignatureView!
    
    var signatureImage : UIImage?
    var orderID = Int()
    
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var clearBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

        
        //signatureView.delegate = self 
        
        setUI()
    }
    
    func setUI(){
        
        doneBtn.layer.masksToBounds = true
        doneBtn.layer.cornerRadius = 10
        doneBtn.layer.borderWidth = 2
        doneBtn.layer.borderColor = UIColor.black.cgColor
        
        clearBtn.layer.masksToBounds = true
        clearBtn.layer.cornerRadius = 10
        clearBtn.layer.borderWidth = 2
        clearBtn.layer.borderColor = UIColor.black.cgColor
        
    }

    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clearBtnAction(_ sender: Any) {
        
        signatureView.clear()
        
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
        
       signatureImage = signatureView.getCroppedSignature()
       
        

        
        itemDeliveredMethod()
    }
    
    
    func itemDeliveredMethod() {
        
        let apiChangeStatus = "\(GlobalURL.baseURL1)\(GlobalURL.driverDeliveredItem)"
//        print(apiChangeStatus)
        
        
        
        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let header = ["Authorization": "Bearer "+token]
        
        let param:[String:Any]=["order_id":"\(orderID)"]
        
//        print(param)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(apiChangeStatus, method: .post, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON { response in
            MBProgressHUD.hideHUD()
            if (response.result.value != nil){
                let swiftyJsonVar = JSON(response.result.value!)
//                print(swiftyJsonVar)
                if swiftyJsonVar["error"] == false {
                    UtilityMethods.hideIndicator()
                    
                    NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)
                      self.navigationController?.popToRootViewController(animated: true)
                    
                } else {
                    UtilityMethods.hideIndicator()
                    
                    
                    
                    let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
            }
            else {
                UtilityMethods.hideIndicator()
                
                
            }
        }
        
    }
    

}
