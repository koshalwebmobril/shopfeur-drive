//
//  DocumentUploadVC.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import YPImagePicker


class DocumentUploadVC: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var tfDate: CustomTextField!
    @IBOutlet weak var documentImage: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    let datePicker = UIDatePicker()
    let toolBar = UIToolbar()
    var imgData = Data()
    var imgString = ""
    var isfromMenu = String()
    
    @IBOutlet weak var backButtonWidth: NSLayoutConstraint!
    var components = DateComponents()
    
    
    var imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        if isfromMenu.isEmpty{
            backButtonWidth.constant = 0.0
            backButton.isHidden = true
        }
        
        self.documentImage.layer.cornerRadius = 5.0
        self.documentImage.layer.masksToBounds = true
        self.documentImage.layer.borderColor = UIColor.gray.cgColor
        self.documentImage.layer.borderWidth = 2.0
        

        self.submitButton.layer.cornerRadius = self.submitButton.frame.height/2
        self.submitButton.layer.masksToBounds = true
        
        self.addImageBtn.layer.cornerRadius = self.addImageBtn.frame.height/2
        self.addImageBtn.layer.masksToBounds = true
        
        self.progressBar.layer.cornerRadius = 5.0;
        self.progressBar.layer.masksToBounds = true
        self.progressBar.clipsToBounds = true
        
        showDatePicker()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(DocumentUploadVC.tapFunction))
//        documentImage.isUserInteractionEnabled = true
//        documentImage.addGestureRecognizer(tap)
        
        
        tfDate.backgroundColor = UIColor.white
        tfDate.layer.cornerRadius = tfDate.frame.height/6
        
        
        tfDate.clipsToBounds = true
        tfDate.layer.borderWidth = 2
        tfDate.layer.borderColor = UIColor(red:0.92, green:0.93, blue:0.99, alpha:1.0).cgColor
        
        
        
        
        }
    
    
    @IBAction func AddImageButtonAction(_ sender: Any) {
        
//        tapFunction()
//    }
//    
//    @objc func tapFunction() {
        
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = true
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .cappedTo(size: 1024)
//        config.library.mediaType = .photo
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.startOnScreen = .library
//        config.screens = [.library, .photo,]
//        config.showsCrop = .none
//        config.wordings.libraryTitle = AppConstants.librarytitle
//        config.hidesStatusBar = true
//        config.hidesBottomBar = false
//        config.library.maxNumberOfItems = 1
//
//        let picker = YPImagePicker(configuration: config)
//        /* Single Photo implementation. */
//        picker.didFinishPicking { [unowned picker] items, _ in
//            self.documentImage.image = items.singlePhoto?.image
//            self.imgString = "image"
//            picker.dismiss(animated: true, completion: nil)
//
//        }
//        picker.didFinishPicking { [unowned picker] items, cancelled in
//            if cancelled {
//               print("Picker was canceled")
//            }
//            picker.dismiss(animated: true, completion: nil)
//        }
//        present(picker, animated: true, completion: nil)
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.documentImage.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
                    self.imgString = "image"
        dismiss(animated: true, completion: nil)
    
    }

    
    @IBAction func action_back(_ sender: Any) {
        
        if isfromMenu.isEmpty {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
     
    func showDatePicker(){
        //Formate Date
        
        components.year = 10
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePicker.minimumDate = NSDate() as Date
        datePicker.maximumDate = maxDate
        datePicker.datePickerMode = .date
        
        
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tfDate.inputAccessoryView = toolbar
        tfDate.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        tfDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func submitDocument(_ sender: Any) {
        
        if (imgString == "") {
            statusBarErrorAlert(msg: AppConstants.UploadDlImage)
        } else if (tfDate.text == "") {
            statusBarErrorAlert(msg: AppConstants.EnterExpiryDate)
        } else {
             self.UPLOD()
        }
       
        
    }
    
    
    
    func UPLOD() {
        
        let apiUploadDocumentURL = "\(GlobalURL.baseURL1)\(GlobalURL.uploadDocumentURL)"

        let parameters = ["license_date": "\(tfDate.text ?? "")"]

        let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""

        imgData = documentImage.image!.jpegData(compressionQuality: 0.4)!

        let url = apiUploadDocumentURL
//        print(url)
//        print(parameters)
        let headers: HTTPHeaders = [
           "Authorization": "Bearer "+token ,
            "Content-type": "multipart/form-data"
        ]
//        print(headers)
        
//        UtilityMethods.showIndicator(withMessage: "Please wait...")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(self.imgData, withName: "driver_license", fileName: "image.png", mimeType: "image/png")

        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    MBProgressHUD.hideHUD()
                    
//                    print("Done")
                    if (response.result.value != nil){
                        let swiftyJsonVar = JSON(response.result.value!)
//                        print(swiftyJsonVar)
                        if swiftyJsonVar["error"] == false {
                            UtilityMethods.hideIndicator()
                            
                            let responceJson = swiftyJsonVar["result"]
//                            print(responceJson)
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
                            action in
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "AddVehicleDetailsVC") as! AddVehicleDetailsVC
                                vc.modalPresentationStyle = .fullScreen
                                vc.isfromMenu = self.isfromMenu
                                self.present(vc, animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            UtilityMethods.hideIndicator()
                            
                            let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                
                }
            case .failure(let error):
                
                UtilityMethods.hideIndicator()
//                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    
    
}
