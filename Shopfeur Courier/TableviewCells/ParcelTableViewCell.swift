//
//  ParcelTableViewCell.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 23/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class ParcelTableViewCell: UITableViewCell {
    
      @IBOutlet weak var backView: UIView!
       @IBOutlet weak var personImage: UIImageView!
       @IBOutlet weak var lblPerson: UILabel!
       
       @IBOutlet weak var btnCall: UIButton!
       @IBOutlet weak var lblAddress: UILabel!
       @IBOutlet weak var addressImage: UIImageView!
       @IBOutlet weak var sourceImageView: UIImageView!
       @IBOutlet weak var sideView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
