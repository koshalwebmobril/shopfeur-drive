//
//  FinalCell.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 23/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class FinalCell: UITableViewCell {
    
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var lblfinalName: UILabel!
    @IBOutlet weak var lblFinalAddress: UILabel!
    
    @IBOutlet weak var FinalBtnCall: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
