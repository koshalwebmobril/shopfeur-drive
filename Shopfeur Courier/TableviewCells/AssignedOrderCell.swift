//
//  AssignedOrderCell.swift
//  Shopfeur Courier
//
//  Created by Sushant on 29/11/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class AssignedOrderCell: UITableViewCell {

    @IBOutlet weak var lbl_orderAmount: UILabel!
    @IBOutlet weak var lbl_OrderId: UILabel!
    @IBOutlet weak var lbl_CustomerName: UILabel!
    @IBOutlet weak var lbl_CustomerPhone: UILabel!
    @IBOutlet weak var lbl_CustomerEmail: UILabel!
    @IBOutlet weak var lbl_CustomerAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
