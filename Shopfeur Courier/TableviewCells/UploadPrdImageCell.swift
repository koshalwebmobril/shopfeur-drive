//
//  UploadPrdImageCell.swift
//  Shopfeur Courier
//
//  Created by Sushant on 29/11/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class UploadPrdImageCell: UITableViewCell {

    @IBOutlet weak var item_Image: UIImageView!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var lbl_ItemDescription: UILabel!
    @IBOutlet weak var lbl_ItemQty: UILabel!
    @IBOutlet weak var lbl_ItemAmount: UILabel!
    @IBOutlet weak var btn_UploadImage: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
