//
//  ProductListCell.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 24/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {
    
    
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
