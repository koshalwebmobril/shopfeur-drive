//
//  VehicleCell.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 14/02/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class VehicleCell: UITableViewCell {
    
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var logoView: UIImageView!
    
    @IBOutlet weak var farelabel: UILabel!
    @IBOutlet weak var bikelabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
