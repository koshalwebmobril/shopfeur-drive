//
//  NotificationsCell.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {
    
    @IBOutlet weak var view_notifications: UIView!
    
    @IBOutlet weak var lblOrderupadtesStatus: UILabel!
    
    @IBOutlet weak var pendingBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
