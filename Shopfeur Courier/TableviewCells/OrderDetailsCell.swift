//
//  OrderDetailsCell.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 24/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class OrderDetailsCell: UITableViewCell {
    
    
    @IBOutlet weak var orderProductView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    
    @IBOutlet weak var uploadImgBtn: UIButton!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDetails: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
