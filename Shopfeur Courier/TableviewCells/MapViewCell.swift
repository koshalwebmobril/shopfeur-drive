//
//  MapViewCell.swift
//  Shopfeur Courier
//
//  Created by Sushant on 29/11/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class MapViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewMapBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
