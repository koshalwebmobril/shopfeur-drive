//
//  OrderHistoryCell.swift
//  Shopfeur Courier
//
//  Created by deepkohli on 21/10/19.
//  Copyright © 2019 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var view_Orderhistory: UIView!
    @IBOutlet weak var img_UserProfile: UIImageView!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_OrderStatus: UILabel!
    @IBOutlet weak var lbl_CustomerName: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_ProductName: UILabel!
    
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var lblorderDelivery: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressDetails: UILabel!
    
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
