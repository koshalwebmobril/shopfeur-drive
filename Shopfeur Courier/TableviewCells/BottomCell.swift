//
//  BottomCell.swift
//  Shopfeur Courier
//
//  Created by Sandeep Kumar on 23/01/2020.
//  Copyright © 2020 ShopfeurCourier.com. All rights reserved.
//

import UIKit

class BottomCell: UITableViewCell {
    
    
    @IBOutlet weak var lblAddressImg: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var addressImageView: UIImageView!
    @IBOutlet weak var btnDelivery: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
