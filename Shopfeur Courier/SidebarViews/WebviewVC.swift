//
//  WebviewVC.swift
//  Shopfeur Users
//
//  Created by deepkohli on 09/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import WebKit

class WebviewVC: UIViewController ,WKNavigationDelegate{

    @IBOutlet weak var webView: WKWebView!
    var labeltitle = String()
    @IBOutlet weak var lbl_title: UILabel!
    var urlString = String()
    var comeFrom = Int()
   
    var isfromSignup :Bool = false
    
    @IBOutlet weak var lineView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_title.text = labeltitle
        lineView.textDropShadow()

        if lbl_title.text == "Privacy Policy"{
            urlString = "https://www.shopfeur.com/api/v1/auth/api_get_pages?page_id=7"
        }
        else if lbl_title.text == "About us"{
            urlString = "https://www.shopfeur.com/api/v1/auth/api_get_pages?page_id=4"
        }
        else if lbl_title.text == "Terms & Conditions"{
            urlString = "https://www.shopfeur.com/api/v1/auth/api_get_pages?page_id=1"
        }
        else{
            urlString = "https://www.shopfeur.com/api/v1/auth/api_get_pages?page_id=1"
        }
        
        let url = URL(string: urlString)
        let requestObj = URLRequest(url: url! as URL)
        webView.navigationDelegate = self
        webView.uiDelegate = self as? WKUIDelegate
        webView.load(requestObj)
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
            MBProgressHUD.hideHUD()
        MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
    MBProgressHUD.hideHUD()
    MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_title.text = labeltitle
        
        
    }
    
    @IBAction func action_back(_ sender: Any) {
        
        if isfromSignup{
            self.dismiss(animated: true, completion: nil)
            
        }else{
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        
         vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
        }
//
    }
    
    
   
   
    
    

}
