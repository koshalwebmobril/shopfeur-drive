//
//  LogoutVC.swift
//  Shopfeur Users
//
//  Created by deepkohli on 17/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LogoutVC: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var butt_logout: UIButton!
    @IBOutlet weak var butt_cancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        butt_logout.layer.cornerRadius = 4.0
        butt_logout.layer.borderWidth = 1.0
        butt_logout.layer.borderColor = UIColor.clear.cgColor
        butt_cancel.layer.cornerRadius = 4.0
        butt_cancel.layer.borderWidth = 1.0
        butt_cancel.layer.borderColor = UIColor.white.cgColor
        
    }
    
    @IBAction func action_logout(_ sender: Any) {
        callLogoutAPI()
    }
    
    @IBAction func action_cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func callLogoutAPI() {
        if Reachability.isConnectedToNetwork() {
            
            
            let token = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
            let apiLogoutURL = "\(GlobalURL.baseURL)\(GlobalURL.logoutURL)"
//            print(apiLogoutURL)
            let header = ["Authorization": "Bearer "+token]

//            print(header)
//            UtilityMethods.showIndicator(withMessage: "Please wait...")
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(apiLogoutURL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
                
                MBProgressHUD.hideHUD()
                
                UtilityMethods.hideIndicator()
                if (response.result.value != nil){
                    let swiftyJsonVar = JSON(response.result.value!)
//                    print(swiftyJsonVar)
                    if swiftyJsonVar["error"] == false {
                        UtilityMethods.hideIndicator()

                        let responceJson = swiftyJsonVar["result"]
//                        print(responceJson)
                        
                        let defaults = UserDefaults.standard
                        defaults.removeObject(forKey: "user_Id")
                        defaults.removeObject(forKey: "authToken")
                        defaults.synchronize()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {action in
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = mainStoryBoard.instantiateViewController(withIdentifier: "SignInVc") as? SignInVc
                            let navigationController = UINavigationController(rootViewController: controller!)
                            navigationController.isNavigationBarHidden = true
                            AppDelegate.sharedInstance.window?.rootViewController = navigationController
                            AppDelegate.sharedInstance.window?.makeKeyAndVisible()
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    } else {
                        UtilityMethods.hideIndicator()
                        
                        let alert = UIAlertController(title: NSLocalizedString("Shopfeur-Courier", comment: ""), message: swiftyJsonVar["message"].string
                            , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            statusBarErrorAlert(msg: "No Internet")
        }
        
    }
    
}
