//
//  AppConstants.swift
//  Shopfeur Users
//
//  Created by deepkohli on 11/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit

class AppConstants: NSObject {

    static let MainStoryboard = "Main"
    static var arr_SplashData = NSArray()
    static let PagerComplete = "PagerComplete"
    static let NoInternet = "No Internet Connectivity."
    static let EmptyPassword = "Password Field is Empty."
    static let EmptyGender = "Gender Field is Empty."
    static let EmptyConfirmPassword = "Confirm Password Field is Empty."
    static let EmptyMobileNumber = "Phone Number Field is Empty."
    static let InvalidMobileNumber = "Invalid Phone Number."
    static let PasswordnotMatch = "Password doesn't match."
    static let SelectProfilePicture = "Please select your Profile Picture."
    static let Emptyname = "Name field is empty."
    static let Alreadyhaveaccount = "Already have an account? "
    static let signin = "Sign in"
    static let librarytitle = "Photos"
    static let Donthaveaccount = "Don't have an account? "
    static let signup = "Sign up"
    static let LoginComplete = "LoginComplete"
    static let EmptyOTP = "Please enter OTP."
    static let InvalidPassword = "Invalid Password"
    static let emptyAge = "Enter your date of birth"
    static let invalidAge = "Must be between 18 and 50 years old"
    static let acceptTerms = "Accept terms and conditions first"
    static let EnterModelName = "Enter Model Name"
    static let EnterYear = "Enter Model Year"
    static let EnterType = "Enter Model Type"
    static let EnterRegistrationNo = "Enter Registration Number"
    static let SelectImages = "Select atleast two images."
    static let UploadInsurance = "Upload Insurance Image"
    static let UploadPoliceClearance = "Upload Police Clearance Image"
    static let UploadWorkPermit = "Upload Your Work Permit Image"
    static let UploadDlImage = "Upload Your Driving License Image"
    static let EnterExpiryDate = "Enter Expiry Date"
    
    static let title = "Please enter title"
    static let description = "Please enter description"
}
