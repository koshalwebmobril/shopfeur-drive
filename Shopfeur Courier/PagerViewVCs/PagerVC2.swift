//
//  PagerVC2.swift
//  Shopfeur Users
//
//  Created by deepkohli on 11/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import SDWebImage

class PagerVC2: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let arr_data = AppConstants.arr_SplashData.object(at: 1) as! NSDictionary
        let imageURL = arr_data.object(forKey: "image") as! String
        imgView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgView.sd_imageTransition = .fade
        imgView.sd_setImage(with: URL(string: APPUrls.ImageBaseUrl + imageURL), placeholderImage: #imageLiteral(resourceName: "PagerThumbnail"))
        lbl_heading.text = arr_data.object(forKey: "heading") as? String ?? "Not Available"
        lbl_description.text = arr_data.object(forKey: "description") as? String ?? "Not Available"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
