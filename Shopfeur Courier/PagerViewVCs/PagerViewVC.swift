//
//  PagerViewVC.swift
//  Shopfeur Users
//
//  Created by deepkohli on 01/10/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit

class PagerViewVC: UIViewController {

    var window: UIWindow?
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var butt_Next: UIButton!
    
    var pagerController: PagerController? {
        didSet {
            pagerController?.pagerDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: AppConstants.PagerComplete)
        UserDefaults.standard.synchronize()
        butt_Next.layer.cornerRadius = butt_Next.frame.height/2
        pageControl.addTarget(self, action: #selector(didChangePageControlValue), for: .valueChanged)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pagerController = segue.destination as? PagerController {
            self.pagerController = pagerController
        }
    }
    
    /* Fired when the user taps on the pageControl to change its current page.*/
    @objc func didChangePageControlValue() {
            pagerController?.scrollToViewController(index: pageControl.currentPage)
    }
    

    @IBAction func action_Next(_ sender: Any) {
        pagerController?.scrollToNextViewController()
        
        if butt_Next.titleLabel?.text == "Start" {
//            print("Start")
            UserDefaults.standard.set(true, forKey: AppConstants.PagerComplete)
            UserDefaults.standard.synchronize()
            let signinvc = SignInRouters.createModule();
            window = UIWindow(frame: UIScreen.main.bounds);
            window?.rootViewController = signinvc
            window?.makeKeyAndVisible();
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PagerViewVC: PagerControllerDelegate {
    
    func pagerController(pagerController: PagerController,
        didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func pagerController(pagerController: PagerController,
        didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
        if pageControl.currentPage == 2 {
            butt_Next.setTitle("Start", for: .normal)
        } else {
            butt_Next.setTitle("Next", for: .normal)
        }
    }
    
}
